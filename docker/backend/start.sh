#!/bin/bash
set -e

/app/docker/backend/wait-for-it.sh ${DB_HOST}:${DB_PORT} --timeout=120

npm install

node ./node_modules/sequelize-auto-migrations/bin/runmigration

npm run start
