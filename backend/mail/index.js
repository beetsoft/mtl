const nodemailer = require("nodemailer");

module.exports = (email, subject, html) => {
  const transporter = nodemailer.createTransport({
    service: "yandex",
    auth: {
      user: "multiusertdl@yandex.ru",
      pass: "1234password"
    }
  });

  const mailOptions = {
    from: "multiusertdl@yandex.ru",
    to: email,
    subject: subject,
    html: html
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};
