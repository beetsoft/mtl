module.exports = class SessionService {
   constructor() {
       this.users = [];
   }

   addUser(socketId, userId) {
       const newUser = {
           socketId: socketId,
           userId: userId
       }
       this.users.push(newUser)
   }

   removeUser(disconnectedUserId) {
       this.users = this.users.filter(user => disconnectedUserId !== user.userId);
   }

   getUsers() {
       return this.users;
   }

   getUser(id) {
       return this.users.find(user => user.userId === id);
   }
}