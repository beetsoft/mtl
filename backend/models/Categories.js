module.exports = (sequelize, DataTypes) => {
    const Categories = sequelize.define("categories", {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING,
            uniq: false,
            allowNull: false
        }
    }, { underscored: true })

    Categories.associate = function(models) {
        models.categories.belongsTo(models.user);
    }

    return Categories
}