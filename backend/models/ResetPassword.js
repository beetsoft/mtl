const bcrypt = require("bcryptjs");
const sendEmail = require("../mail");
const moment = require("moment");

module.exports = (sequelize, DataTypes) => {
    const ResetPassword = sequelize.define("reset_password", {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
      },
      hash: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
      }
    }, { underscored: true });

    ResetPassword.changePasswordRequest = function(data) {
      const errors = [];

      return sequelize.models.user
        .findOne({
          where: {
            email: data.email
          }
        })
        .then(user => {
          if (user) {
            const salt = bcrypt.genSaltSync(10);
            const userHash = bcrypt.hashSync(Date.now().toString(), salt);

            return this
              .create({ hash: userHash, email: user.email, user_id: user.id })
              .then(data => {
                sendEmail(
                  user.email,
                  "Recover your password",
                  `Follow this <a href="localhost/${"#" +
                    userHash}">link</a> to change your password.`
                );

                return Promise.resolve(data);

              });
          } else {
            errors.push({
              context: {
                label: "email"
              },
              type: "not_exist"
            });

            return Promise.reject(errors);

          }
        });
    }

    ResetPassword.changePassword = function(data) {

      const errors = [];

      return this
        .findOne({
          where: {
            hash: data.hash
          },
          include: [ { model: sequelize.models.user } ]
        })
        .then(password => {

          if (password) {
            if (moment().diff(password.created_at, "minutes") >= 10) {
              errors.push({
                context: {
                  label: "password"
                },
                type: "password_link_expired"
              });

              return password.destroy().then(() => {
                return Promise.reject(errors)

              });
            } else {

              const salt = bcrypt.genSaltSync(10);
              const passwordHash = bcrypt.hashSync(data.password, salt);
              data.password = passwordHash;

              return password.user.update({
                password: data.password
              })
              .then(() => {

               return password.destroy()
              });
            }
          } else {
            
            errors.push({
              context: {
                label: "password"
              },
              type: "invalid_link"
            });

            return Promise.reject(errors)
          }
        });
    }

    ResetPassword.associate = function(models) {
      models.reset_password.belongsTo(models.user);
    };

    return ResetPassword;
  };