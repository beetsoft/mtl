const sendEmail = require("../mail");
const bcrypt = require("bcryptjs");
const moment = require("moment");

module.exports = (sequelize, DataTypes) => {
  const ConfirmEmail = sequelize.define("confirm_email", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    hash: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, { underscored: true });

  ConfirmEmail.createConfirmEmailRequest = function(data) {

    const salt = bcrypt.genSaltSync(10);
    const emailHash = bcrypt.hashSync(Date.now().toString(), salt);

  return this
    .create({
      hash: emailHash,
      email: data.email,
      user_id: data.user_id
    })
    .then(request => {
      sendEmail(
        data.email,
        "Confirm your email",
        `<h1>Hello!</h1><p>To confirm you adress follow this <a href='localhost/confirm_email?hash=${emailHash}'>link</a></p>`
      );

      return Promise.resolve(request)
    });
  }



  ConfirmEmail.confirmEmailRequest = function(data) {

    return this.findOne({
      where: {
        email: data.email
      }
    }).then(existing_request => {

      let promise = Promise.resolve();
      if (existing_request) {
        promise = existing_request.destroy();
        data.user_id = existing_request.user_id;
      } 

      return promise.then(() => {
        return this.createConfirmEmailRequest(data);
      })
    });
  };

  ConfirmEmail.confirmEmail = function(data) {
    const errors = [];

      return this
        .findOne({
          where: {
            hash: data.hash
          },
          include: [ { model: sequelize.models.user, as: "user" } ]
        })
        .then(data => {
          if (moment().diff(data.created_at, "hours") >= 1) {
            //Link expired
            errors.push({
              context: {
                label: "email"
              },
              type: "email_link_expired"
            });

            data.destroy().then(() => {

              return Promise.reject(errors);

            });
          } else {

            return data.user.update({ is_active: true }).then(() => {
              return data.destroy()
            });

          }
        })
        .catch(error => {
          errors.push({
            context: {
              label: "email"
            },
            type: "invalid_link"
          });

          return Promise.reject(errors)

        });
  }

  ConfirmEmail.associate = function(models) {
    models.confirm_email.belongsTo(models.user);
  };

  return ConfirmEmail;
};
