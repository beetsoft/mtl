module.exports = (sequelize, DataTypes) => {
  const TodoItem = sequelize.define(
    "todo_item",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      text: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      is_done: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      }
    },
    { underscored: true }
  );

  TodoItem.associate = function(models) {
    models.todo_item.belongsTo(models.todo_list);
  };

  return TodoItem;
};
