const UserManager = require('../server');

module.exports = (sequelize, DataTypes) => {
  const TodoList = sequelize.define(
    "todo_list",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      title: {
        type: DataTypes.TEXT
      }
    },
    { underscored: true }
  );

  TodoList.checkOwner = function(data) {
    const errors = [];
    return this.findById(data.id).then(list => {
      
      if (list.user_id !== data.user_id) {
        const error = {
          context: {
            label: "permissions"
          },
          type: "no_permissions"
        };

        errors.push(error)
        return Promise.reject(errors);
      } else {
        return list;
      }
    });
  };

  TodoList.createList = function(data, user) {
    return this.create({ title: data.title, user_id: user.id })
  };

  TodoList.editListTitle = function(data, user) {
    const errors = [];

    return this.findById(data.id).then(list => {
      if (user.id !== list.user_id) {
        errors.push({
          context: {
            label: "permission"
          },
          type: "no_permisson"
        });

        return Promise.reject(errors);
      } else {
        return list.update({ title: data.title })
      }
    });
  };

  TodoList.deleteTodoList = function(data, user) {
    const errors = [];

    return this.findOne({
      where: {
        id: data.id
      }
    }).then(list => {
      if (list.user_id !== user.id) {
        errors.push({
          context: {
            label: "permission"
          },
          type: "no_permisson"
        });

        return Promise.reject(errors);
      } else {

       return list.destroy().then(() => list.id)
      }
    });
  };

  TodoList.getTodoList = function(data, user) {
    const errors = [];    
    return this.findOne({
      where: {
        id: data.id
      },
      include: [{ model: sequelize.models.todo_item }, { model: sequelize.models.user }]
    }).then(list => {        
      return sequelize.models.todo_assigned_users.findAll({ where: { todo_list_id: data.id }, include: [{ model: sequelize.models.user }] }).then(assigned => {  
                
        const assignedIds = assigned.map(user => user.user_id);          
        list.dataValues.assigned_users = assigned.map(assigned => assigned.user);
        if(list.dataValues.assigned_users.filter(assignedUser => assignedUser.id === user.id).length === 0) {
          list.dataValues.assigned_users.push(user)
        } else {
          list.dataValues.assigned_users.push(list.user)
        }

        return sequelize.models.invited_users.findAll({ where: { todo_list_id: data.id } }).then(invited_users => {
          if(invited_users) {
            list.dataValues.invited_users = invited_users;
          }

          if (
            assignedIds.filter(assignedUser => assignedUser=== user.id).length > 0 || user.id === list.user_id
          ) {
            return Promise.resolve(list);          

          } else {
            errors.push({
              context: {
                label: "permission"
              },
              type: "no_permisson"
            });         

            return Promise.reject(errors);
          }  
          
        })

      
        })
    });
  };

  TodoList.getAssignedUsers = function(data) {

    return sequelize.models.todo_assigned_users
      .findAll({
        where: {
          todo_list_id: data.todo_list_id
        },
        include: [{ model: sequelize.models.user }]
      })
      .then(assigned => {
        const users = assigned.map(todo => {
          const user = todo.user;
          return user;
        });

        return Promise.resolve(users);
      })
  };

  TodoList.deleteShare = function(data, user) {
    const errors = [];

    return this.findById(data.todo_list_id).then(list => {

      if(data.invited) {
        return sequelize.models.invited_users.findOne({ where: { id: data.user_id } }).then(invited_user => {
          return invited_user.destroy().then(() => {
            return {
              userId: invited_user.id,
              todo_list_id: data.todo_list_id,
              invited: true
            }
          })  
        })
      }

      return list.getTodo_assigned_users().then(assigned => {
        
        let assignedUser;
        if(assigned.length > 0) {
          assignedUser = assigned.find(assignedUser => assignedUser.user_id === data.user_id);
        }
  

        if(assignedUser && assignedUser.user_id === user.id || list.user_id === user.id) {

          return assignedUser.destroy().then(() => {
           const userForDeleteShareConnectionId = UserManager.getUser(assignedUser.user_id);
            return {
              userId: assignedUser.user_id,
              todo_list_id: data.todo_list_id,
              connectionId: userForDeleteShareConnectionId
            }
          })
        } else {
          
          errors.push({
            context: {
              label: "permission"
            },
            type: "no_permisson"
          });

          return Promise.reject(errors);
        }

      })
    })
  }

  //Todo item

  TodoList.addTodo = function(data, user) {

    return this.checkOwner({ id: data.todo_list_id, user_id: user.id })
      .then(() => {
        return sequelize.models.todo_item
          .create({
            todo_list_id: data.todo_list_id,
            text: data.text
          }).then(() => sequelize.models.todo_list.getTodoList({ id: data.todo_list_id }, user))
      })
  };

  TodoList.editTodoText = function(data, user) {
    const errors = [];

    return sequelize.models.todo_item
      .findById(data.id)
      .then(todoItem => {
        return todoItem.getTodo_list().then(list => {

          if (list.user_id !== user.id) {
            errors.push({
              context: {
                label: "permissions"
              },
              type: "no_permissions"
            });
            return Promise.reject(errors);
          } else {
            
            return todoItem.update({ text: data.text }).then(() => sequelize.models.todo_list.getTodoList({ id: todoItem.todo_list_id }, user));
          }

        })
      });
  };

  TodoList.editTodoStatus = function(data, user) {
    const errors = [];

    return sequelize.models.todo_item
      .findOne({
        where: {
          id: data.id
        },
        include: [
          {
            model: sequelize.models.todo_list,
            include: [{ model: sequelize.models.todo_assigned_users }]
          }
        ]
      })
      .then(todoItem => {
        const assignedUsersIds = todoItem.todo_list.todo_assigned_users.map(
          assigned => assigned.user_id
        );

        if (
          !assignedUsersIds.some(id => id === user.id) &&
          todoItem.todo_list.user_id !== user.id
        ) {
          errors.push({
            context: {
              label: "permission"
            },
            type: "no_permisson"
          });

          return Promise.reject(errors);
        } else {
          return todoItem.update({ is_done: !todoItem.is_done }).then(() => sequelize.models.todo_list.getTodoList({ id: todoItem.todo_list_id }, user));
        }
      });
  };

  TodoList.deleteTodo = function(data, user) {
    const errors = [];
    return sequelize.models.todo_item
      .findOne({
        where: {
          id: data.id
        },
        include: [{ model: sequelize.models.todo_list }]
      })
      .then(todo => {
        if (todo.todo_list.user_id !== user.id) {
          errors.push({
            context: {
              label: "permission"
            },
            type: "no_permisson"
          });

          return Promise.reject(errors);
        } else {
          return todo.destroy().then(() => sequelize.models.todo_list.getTodoList({ id: todo.todo_list_id }, user));
        }
      });
  };

  TodoList.editFavoriteStatus = function (data, user) {
    return sequelize.models.favorite_list.findOne({ where: {user_id: user.id, todo_list_id: data.id } }).then(favorite => {
      if(favorite) {
        return favorite.destroy().then(() => {

          return Promise.resolve(favorite.todo_list_id)

        })
      } else {
        return sequelize.models.favorite_list.create({user_id: user.id, todo_list_id: data.id }).then(favorite => {

          return Promise.resolve(favorite.todo_list_id)
          
        })
      }
    })
  }
          
  TodoList.duplicateList = function (data, user) {
    const errors = [];
    return this.findOne({ where: { id: data.id }, include: [ { model: sequelize.models.todo_assigned_users }, { model: sequelize.models.todo_item } ] }).then(list => {
      if(user.id !== list.user_id && list.todo_assigned_users.filter(assigned => assigned.user_id === user.id).length === 0) {
        errors.push({
          context: {
            label: "permission"
          },
          type: "no_permisson"
        });
        return Promise.reject(errors);
      } 

      return this.createList({ title: list.title }, user).then(duplicatedList => {

        return sequelize.models.todo_item.bulkCreate(list.todo_items.map(item => {
          return {
            todo_list_id: duplicatedList.id,
            text: item.text,
            is_done: item.is_done
          }
        })).then(items => {

          duplicatedList.dataValues.todo_items = items;

          return Promise.resolve(duplicatedList);
        })
      })
    })
  }

  TodoList.addCategoryToList = function (data, user) {

    if(!data.title) {
      return sequelize.models.todo_list.findOne({ where: { id: data.todo_list_id } }).then(list => {
        return list.update({ category_id: null }).then(() => Promise.resolve())
      })
    }

    return sequelize.models.categories.findOne({ where: { user_id: user.id, title: data.title} }).then(category => {

      let promise = Promise.resolve();

      if(!category) {
        promise = sequelize.models.categories.create({ user_id: user.id, title: data.title }).then(newCategory => {
          category = newCategory
        })
      } 

     return promise.then(() => {
        return sequelize.models.todo_list.findOne({ where: { id: data.todo_list_id } }).then(list => {
          return list.update({ category_id: category.id }).then(() => Promise.resolve(category))
        })
      }) 
    })
  }

  TodoList.editCategory = function (data) {
    return sequelize.models.categories.findOne({ where: { id: data.category_id } }).then(category => category.update({ title: data.title }))
  }

  TodoList.deleteCategory = function (data) {
    return sequelize.models.categories.findById(data.category_id).then(category => category.destroy())
  }
  
  TodoList.associate = function(models) {
    models.todo_list.hasMany(models.todo_item, { onDelete: 'cascade', hooks: true });
    models.todo_list.hasMany(models.todo_assigned_users, { onDelete: 'cascade', hooks: true });
    models.todo_list.hasMany(models.invited_users, { onDelete: 'cascade', hooks: true })
    models.todo_list.belongsTo(models.user);
    models.todo_list.hasMany(models.favorite_list, { onDelete: 'cascade', hooks: true });
    models.todo_list.belongsTo(models.categories)
  };

  return TodoList;
};
