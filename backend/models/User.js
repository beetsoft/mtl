const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const key = require("../keys").passport_key;
const moment = require("moment");
const stripe_key = require("../keys").stripe_key;
const stripe = require("stripe")(stripe_key);
const UserManager = require('../server');
const sendEmail = require("../mail");

const payment = (token) => {
  return stripe.charges
    .create({
      amount: 50,
      currency: "usd",
      description: "Paid plan",
      source: token
    })
    .then(res => {

      const paymentData = {
        token: res.id,
        amount: res.amount,
        currensy: res.currency,
        receipt_url: res.receipt_url,
        paid: res.paid,
        created: res.created
      }
      
      return paymentData
    })
}

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "user",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      email: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          msg: "Email address already in use!"
        },
        allowNull: false
      },
      firstname: {
        type: DataTypes.STRING
      },
      lastname: {
        type: DataTypes.STRING
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      is_free: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      address: {
        type: DataTypes.TEXT
      },
      color: {
        type: DataTypes.STRING,
        defaultValue: function() {
          const colors = ['2BB697', '4A90E2', '133440', '268790', 'C9E3D8'];
          return colors[Math.floor(Math.random() * colors.length)];
        }
      }
    },
    { underscored: true }
  );

  User.registerUser = function(data) {
    const salt = bcrypt.genSaltSync(10);

    data = Object.assign({}, data)

    data.password = bcrypt.hashSync(data.password.toString(), salt);

    let promise = Promise.resolve();
    
    let paymentData;

    if (!data.is_free && data.stripeToken) {
      promise = payment(data.stripeToken)
        .then(response => {
          data.is_free = false;
          paymentData = response;
        })
    }

    return promise.then(() => {
      return this.create(data)
        .then(user => {
          delete data.stripeToken;

          
          if(!data.is_free) {
            paymentData.user_id = user.id;
            sequelize.models.payment_data.create(paymentData);
          }

          const emailData = {
            email: data.email,
            user_id: user.id
          };
          return sequelize.models.confirm_email
            .confirmEmailRequest(emailData)
            .then(() => {
              return Promise.resolve(user);
            });
        })
    });
  };

  User.login = function(data) {
    const errors = [];
    return this.findOne({
      where: {
        email: data.email
      }
    })
      .then(user => {
        if(!user) {
          errors.push({
            context: {
              label: "email"
            },
            type: "not_exist"
          });
  
          return Promise.reject(errors);
        }

        const isValid = bcrypt.compareSync(
          data.password.toString(),
          user.password
        );

        if (!user.is_active) {
          //Email is not confirmed
          errors.push({
            context: {
              label: "email"
            },
            type: "inactive"
          });

          return Promise.reject(errors);
        }

        if (isValid) {
          const userData = {
            adress: user.adress,
            is_active: user.is_active,
            is_free: user.is_free,
            lastname: user.lastname,
            firstname: user.firstname,
            email: user.email,
            id: user.id
          };

          const expires = data.remember ? 7 : 1;

          const token = jwt.sign({ id: user.id }, key, {
            expiresIn: expires + "d"
          });

          userData.token = {
            key: token,
            expiresIn: moment()
              .add(expires, "d")
              .valueOf()
          };

          return Promise.resolve(userData)
          
        } else {
          //Incorrect password
          errors.push({
            context: {
              label: "password"
            },
            type: "incorrect"
          });

          return Promise.reject(errors);
        }
      })
  };

  User.checkEmail = function(data) {
    const errors = [];
    return this.count({
      where: {
        email: data.email
      }
    }).then(email => {
      if (email > 0) {
        //Email taken
        errors.push({
          context: {
            label: "email"
          },
          type: "taken"
        });
        return Promise.reject(errors);
      } else {
        return Promise.resolve(true);
      }
    });
  };

  User.prototype.editUserInfo = function(data) {
    return this.update(data).then(user => {
      return Promise.resolve(user);
    })
  };

  User.prototype.getAllTodoLists = function() {
    let result = [];
    return sequelize.models.todo_list
      .findAll({
        where: {
          user_id: this.id
        },
        include: [{ model: sequelize.models.todo_item }]
      })
      .then(ownTodos => {
        result = result.concat(ownTodos);

        return sequelize.models.todo_assigned_users
          .findAll({
            where: {
              user_id: this.id
            },
            include: [
              {
                model: sequelize.models.todo_list,
                include: [{ model: sequelize.models.todo_item }, { model: sequelize.models.user }],
              }
            ]
          })
          .then(assigned => {
              const formatAssigned = assigned.map(list => list.todo_list);
              result = result.concat(formatAssigned);
              return sequelize.models.todo_assigned_users.findAll({ where: { todo_list_id: result.map(todo => todo.id) }, include: [ { model: sequelize.models.user } ] })
              .then(users => {
                
                return sequelize.models.invited_users.findAll({ where: { todo_list_id: result.map(todo => todo.id) } }).then(invited_users => {

                  result = result.map(list => {
                    list.dataValues.assigned_users = users.filter(assigned => assigned.todo_list_id === list.id).map(user => user.user)
                    list.dataValues.invited_users  = invited_users.filter(user => user.todo_list_id === list.id)
                    if(list.dataValues.assigned_users.map(user => user.id).filter(id => id === this.id).length === 0) {
                      list.dataValues.assigned_users.push(this)
                    }

                    if(list.user) {
                      list.dataValues.assigned_users.push(list.user)
                    }

                    return list;
                  })
                  return Promise.resolve(result);
                })
                
              })
          });
      });
  };

  User.getUsersByEmail = function(data) {
    return sequelize.models.user
      .findAll({
        where: {
          email: {
            $like: '%' + data.email + '%'
          }
        }
      })
      .then(users => {
        let formattedUsers = [];
        
        if(users.length > 0) {
          formattedUsers = users.map(user => {
            return {
              email: user.email,
              firstname: user.firstname,
              lastname: user.lastname
            }
          });
        }

        return formattedUsers
      });
  };

  User.prototype.upgradeUserAccount = function (data, user) {
    return payment(data.token).then(paymentData => {
      paymentData.user_id = user.id;

      return sequelize.models.payment_data.create(paymentData).then(() => {
        return user.update({ is_free: false }).then(() => {
          return Promise.resolve()
        })
      })
    })
  }

  User.prototype.shareList = function(data) {
    const errors = [];
      if (this.is_free) {
        errors.push({
          context: {
            label: "free_user"
          },
          type: "payment"
        });

        return Promise.reject(errors);

      } else {

        if(data.email === this.email) {
          errors.push({
            context: {
              label: "own_list"
            },
            type: "own_list"
        });
  
          return Promise.reject(errors);
        }

        return sequelize.models.user.findOne({ where: {
            email: data.email
        } }).then(userForShare => {

          if(userForShare) {

            return sequelize.models.todo_list.findOne({ where: { id: data.todo_list_id } }).then(list => {

              if(list.user_id === userForShare.id) {
                errors.push({
                    context: {
                      label: "own_list"
                    },
                    type: "own_list"
                });
  
              return Promise.reject(errors);
              }
  
              return sequelize.models.todo_assigned_users.findOne({where: {
                todo_list_id: data.todo_list_id,
                user_id: userForShare.id
            }}).then(existingUser => {
                  if(existingUser) {
                    errors.push({
                      context: {
                        label: "list"
                      },
                      type: "already_shared"
                  });

                  return Promise.reject(errors)
                } else {

                  return sequelize.models.todo_assigned_users.create({ 
                    todo_list_id: data.todo_list_id,
                    user_id: userForShare.id }).then(() => {

                      return sequelize.models.todo_list.getTodoList({ id: data.todo_list_id }, this).then(list => {

                        sendEmail(
                          userForShare.email,
                            `${this.firstname} ${this.lastname} shared his todo list with you`,
                            `<h1>Hello!</h1><p>Todo list was shared.</p> `
                        );

                        const userForShareConnectionId = UserManager.getUser(userForShare.id);

                        if (userForShareConnectionId){
                          list.userForShareConnectionId = userForShareConnectionId;
                        }
                        
                        list.dataValues.shared_user_id = userForShare.id
                        return Promise.resolve(list)
                      })
                    })

                }
            })
  
            })

          } else {

              return sequelize.models.invited_users.findAll({ where: { email: data.email } }).then(existingInvitedUser => {
                if(existingInvitedUser.length > 0) {
                  if(existingInvitedUser.filter(list => list.todo_list_id === data.todo_list_id).length > 0) {
                    errors.push({
                      context: {
                        label: "list"
                      },
                      type: "already_shared"
                    })
                    return Promise.reject(errors)
                  }
                  return sequelize.models.invited_users.create({
                    hash: existingInvitedUser[0].hash,
                    email: data.email,
                    todo_list_id : data.todo_list_id 
                  }).then(() => {
                    sendEmail(
                      data.email,
                        `${this.firstname} ${this.lastname} shared his todo list with you`,
                        `<h1>Hello!</h1><p>Follow this <a href='localhost/invite?hash=${existingInvitedUser[0].hash}'>link</a></p> to join. `
                    );

                    return sequelize.models.todo_list.getTodoList({id: data.todo_list_id} , this).then(list => {
                      list.dataValues.shared_user_id = existingInvitedUser[0].id
                      return Promise.resolve(list);
                    })
                  })
                } else {

                  const salt = bcrypt.genSaltSync(10);
                  const inviteHash = bcrypt.hashSync(Date.now().toString(), salt);

                  return sequelize.models.invited_users.create({
                    hash: inviteHash,
                    email: data.email,
                    todo_list_id : data.todo_list_id 
                }).then(invited_user => {
  
                      sendEmail(
                        invited_user.email,
                          `${this.firstname} ${this.lastname} shared his todo list with you`,
                          `<h1>Hello!</h1><p>Follow this <a href='localhost/invite?hash=${inviteHash}'>link</a></p> to join. `
                        );
  
                        return sequelize.models.todo_list.getTodoList({id: data.todo_list_id} , this).then(list => {
                          list.dataValues.shared_user_id = invited_user.id
                          return Promise.resolve(list);
                        })
                })
                }
              })
          }
        })
      }
  }

  User.prototype.getFavoriteLists = function() {
    return sequelize.models.favorite_list.findAll({where: { user_id: this.id }}).then(favorites => {
      return Promise.resolve(favorites)
    })
  }

  User.prototype.changePasswordFromProfile = function (data) {
    const errors = [];
    const isValid = bcrypt.compareSync(
      data.current_password.toString(),
      this.password
    );

    if(isValid) {
      const salt = bcrypt.genSaltSync(10);

      data.new_password = bcrypt.hashSync(data.new_password.toString(), salt);

      return this.update({ password: data.new_password })
    } else {
      errors.push({
        context: {
          label: "password"
        },
        type: "incorrect"
      });

      return Promise.reject(errors);
    }
  }

  User.associate = function(models) {
    models.user.hasMany(models.todo_list);
    models.user.hasMany(models.favorite_list)
    models.user.hasMany(models.categories)
  };

  return User;
};
