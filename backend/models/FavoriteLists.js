module.exports = (sequelize, DataTypes) => {
    const FavoriteList = sequelize.define("favorite_list", { 
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        }
     }, { underscored: true })

     FavoriteList.associate = function(models) {
        models.favorite_list.belongsTo(models.user);
        models.favorite_list.belongsTo(models.todo_list);
      };

    return FavoriteList
}