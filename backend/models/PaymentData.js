module.exports = (sequelize, DataTypes) => {
  const PaymentData = sequelize.define("payment_data", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    amount: {
      type: DataTypes.STRING,
      allowNull: false
    },
    currensy: {
      type: DataTypes.STRING,
      allowNull: false
    },
    receipt_url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    paid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    created: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, { underscored: true });


  PaymentData.associate = function(models) {
    models.payment_data.belongsTo(models.user);
  };

  return PaymentData;
};
