module.exports = (sequelize, DataTypes) => {
    const InvitedUsers = sequelize.define("invited_users", {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      hash: {
        type: DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },color: {
        type: DataTypes.STRING,
        defaultValue: function() {
          const colors = ['2BB697', '4A90E2', '133440', '268790', 'C9E3D8'];
          return colors[Math.floor(Math.random() * colors.length)];
        }
      }
    }, { underscored: true });

    InvitedUsers.registerByInvite = function(data) {
      const errors = [];

      return this
        .findAll({
          where: {
            hash: data.hash
          }
        })
        .then(invitedUser => {

          if (invitedUser.length === 0) {
            errors.push({
              context: {
                label: "link"
              },
              type: "invalid_link"
            });

            return Promise.reject(errors)

          } else {
            
           data.email = invitedUser[0].email;
           return sequelize.models.user.registerUser(data).then(user => {

              return sequelize.models.todo_assigned_users
                .bulkCreate(invitedUser.map(share => {
                  return {
                    todo_list_id: share.todo_list_id,
                    user_id: user.id
                  }
                }))
                .then(() => {
                  return this.destroy({ where: { email: invitedUser[0].email } }).then(() => {
                    return Promise.resolve(user);
                  })
                });
            });
          }
        });

    }

    InvitedUsers.associate = function(models) {
      models.invited_users.belongsTo(models.todo_list);
    };

    return InvitedUsers;
  };