module.exports = (sequelize, DataTypes) => {
  const AssignedUsers = sequelize.define("todo_assigned_users", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    }
  }, { underscored: true });

  AssignedUsers.associate = function(models) {
    models.todo_assigned_users.belongsTo(models.todo_list);
    models.todo_assigned_users.belongsTo(models.user)
  };

  return AssignedUsers;
};
