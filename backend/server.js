const SessionService = require('./services/SessionService');
const UserManager = new SessionService();
module.exports = UserManager;
const express = require("express");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const port = process.env.PORT || 3000;
const key = require("./keys").passport_key;
const commands = require("./commands");
const Joi = require("joi");
const models = require('./models');

//Socket
const jwtAuth = require("socketio-jwt-auth");
io.use(
  jwtAuth.authenticate(
    {
      secret: key,
      succeedWithoutToken: true
    },
    (payload, done) => {
      if (payload && payload.id) {
        models.user.findById(payload.id).then(user => {
          return done(null, user);
        })

      } else {
        return done();
      }
    }
  )
);

io.on("connection", socket => {
  if (socket.request.user.id) {
      UserManager.addUser(socket.id, socket.request.user.id)
  }

  Object.keys(commands).map(command => {
    socket.on(command, (data, response) => {

      //Validation
      if (commands[command].validation) {
        const errors = Joi.validate(data, commands[command].validation, {
          abortEarly: false
        });
        if (errors.error) {
          return response({ isSuccess: false, data: { errors } });
        }
      }



      //Authentication
      if (commands[command].private && !socket.request.user.id) {
        const errors = [];
        errors.push({
          context: {
            label: "auth"
          },
          type: "auth_error"
        });
        response({ isSuccess: false, data: { errors } });
      } else {
        if (commands[command].private) {

          if(commands[command].broadcasting) {
            commands[command].do(data, response, socket, socket.request.user, io);
          } else {
            commands[command].do(data, response, socket, socket.request.user);
          }
          
        } else {
          commands[command].do(data, response, socket);
        }
      }
    });
  });

  socket.on('disconnect', () => {
    UserManager.removeUser(socket.request.user.id);
  })

});


http.listen(port, () => {
  console.log(`Server listening at port ${port}`);
});
