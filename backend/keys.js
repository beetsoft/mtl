module.exports = {
    passport_key: process.env.PASSPORT_KEY || 'secret_key',


    db: {
        host: process.env.DB_HOST || 'localhost',
        database: process.env.DB_DATABASE || 'mtl',
        username: process.env.DB_USERNAME || 'mtl',
        password: process.env.DB_PASSWORD || 'secret',
    },
    stripe_key: process.env.STRIPE_KEY
};