const Joi = require("joi");
const models = require("../models");
const roomCommands = require('./roomCommands');

module.exports = {
  /**
   * @access private
   * @param title - todo list name
   */
  createList: {
    do: (data, response, socket, user) => {
        models.todo_list.createList(data, user).then(list => {
          socket.join(list.id)
          return response({ isSuccess: true, data: list });

        }).catch(errors => {
          return response({ isSuccess: false, data: errors })

        })
    },
    private: true,
    validation: {
      title: Joi.string()
        .required()
    }
  },
  /**
   * @access private
   * @param {number} id
   * @param {string} title - todo list name
   */
  editListTitle: {
    do: (data, response, socket, user, io) => {

      models.todo_list.editListTitle(data, user).then(list => {
        io.in(list.id).emit(roomCommands.listEdited, list)
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors });

      })

    },
    private: true,
    validation: {
      title: Joi.string().required(),
      id: Joi.number().required()
    },
    broadcasting: true
  },
  /**
   * @access private
   * @param {number} id - list id
   */
  deleteList: {
    do: (data, response, socket, user, io) => {

        models.todo_list.deleteTodoList(data, user).then(id => {
          io.in(id).emit(roomCommands.listDeleted, id)
          socket.leave(id)
        })
        .catch(errors => {
          return response({ isSuccess: false, data: errors })
        })
    },
    private: true,
    validation: {
      id: Joi.number().required()
    },
    broadcasting: true
  },
  /**
   * @access private
   * Assigned and own todo lists for current user
   */
  getTodoLists: {
    do: (data, response, socket, user) => {
      user.getAllTodoLists().then(lists => {

        if(lists.length > 0) {
          lists.forEach(room => {
            socket.join(room.id)
          })
        }

        return response({ isSuccess: true, data: lists });
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors });
      })
    },
    private: true 
  },

  /**
   * @access private
   * @param {number} id - todo list id
   */
  getTodoList: {
    do: (data, response, socket, user) => {

      models.todo_list.getTodoList(data, user).then(list => {
        socket.join(list.id)

        return response({ isSuccess: true, data: list })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      id: Joi.number().required()
    }
  },

   /**
   * Stop sharing todo list to user
   * @access private
   * @param {number} id - todo list id
   */

  deleteShare: {
    do: (data, response, socket, user, io) => {
      models.todo_list.deleteShare(data, user).then(user => {
        io.in(user.todo_list_id).emit(roomCommands.shareDeleted, user)
        if(user.connectionId) {
          io.sockets.sockets[user.connectionId.socketId].leave(user.todo_list_id)
        }
        
      }).catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      user_id: Joi.number().required(),
      todo_list_id: Joi.number().required(),
      invited: Joi.boolean().required()
    },
    broadcasting: true
  },

   /**
   * Duplicate own/shared list
   * @access private
   * @param {number} id - todo list id
   */
  duplicateList: {
    do: (data, response, socket, user) => {
      models.todo_list.duplicateList(data, user).then(list => {
        socket.join(list.id)
        return response({ isSuccess: true, data: list })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      id: Joi.number().required()
    }
  },
  editFavoriteStatus: {
    do: (data, response, socket, user) => {
      models.todo_list.editFavoriteStatus(data, user).then(listId => {
        return response({ isSuccess: true, data: listId })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      id: Joi.number().required()
    }
  },

  addCategoryToList: {
    do: (data, response, socket, user) => {
      models.todo_list.addCategoryToList(data, user).then(category => {
        return response({ isSuccess: true, data: category })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      title: Joi.string().allow('').optional(),
      todo_list_id: Joi.number().required()
    }
  },

  editCategory: {
    do: (data, response, socket, user) => {
      models.todo_list.editCategory(data).then(category => {
        return response({ isSuccess: true, data: category })
      })
      .catch(errors => {
        return response({ isSuccess: true, data: errors })
      })
    },
    private: true,
    validation: {
      title: Joi.string().required(),
      category_id: Joi.number().required()
    }
  },

  deleteCategory: {
    do: (data, response, socket) => {
      models.todo_list.deleteCategory(data).then(category => {
        return response({ isSuccess: true, data: category })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      category_id: Joi.number().required()
    }
  }
};
