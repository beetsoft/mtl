module.exports = {
  //Todo events
  todoUpdated: "todoUpdated",
  todoDeleted: "todoDeleted",
  todoCreated: "todoCreated",
  //Todo list events
  listEdited: "listEdited",
  listDeleted: "listDeleted",
  listShared: "listShared",
  shareDeleted: "shareDeleted"
};
