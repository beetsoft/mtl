const models = require("../models");
const Joi = require("joi");
const roomCommands = require('./roomCommands');

module.exports = {
  /**
   * @access private
   * @param {number} id - todo id
   * @param {string} text 
   */
  addTodo: {
    do: (data, response, socket, user, io) => {

      models.todo_list.addTodo(data, user).then(list => {
        io.in(list.id).emit(roomCommands.todoCreated, list)
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      text: Joi.string().required(),
      todo_list_id : Joi.number().required()
    },
    broadcasting: true
  },

  /**
   * @access private
   * @param {number} id - todo id
   * @param {string} text 
   */

  editTodoText: {
    do: (data, response, socket, user, io) => {
      models.todo_list.editTodoText(data, user).then(list => {
        io.in(list.id).emit(roomCommands.todoUpdated, list)
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors });
      })
    },
    private: true,
    validation: {
      text: Joi.string().required(),
      id : Joi.number().required()
    },
    broadcasting: true
  },
  /**
   * @access private
   * @param {number} id - todo id
   */
  editTodoStatus: {
    do: (data, response, socket, user, io) => {

      models.todo_list.editTodoStatus(data, user).then(list => {
        io.in(list.id).emit(roomCommands.todoUpdated, list)

      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })

      });

    },
    private: true,
    validation: {
      id: Joi.number().required()
    },
    broadcasting: true
  },

  /**
   * @access private
   * @param {number} id - todo id
   */
  deleteTodo: {
    do: (data, response, socket, user, io) => {

      models.todo_list.deleteTodo(data, user).then(list => {
        io.in(list.id).emit(roomCommands.todoDeleted, list)

      }).catch(errors => {
        return response({ isSuccess: false, data: errors })
        
      })
    },
    private: true,
    validation: {
      id: Joi.number().required()
    },
    broadcasting: true
  }
};
