const models = require("../models");
const Joi = require("joi");


module.exports = {
  /**
   * Email confirmation
   * @access public
   * @param {string} email
   */
  confirmEmailRequest: {
    do: (data, response) => {
      models.confirm_email.confirmEmailRequest(data).then(() => {
        return response({ isSuccess: true });

      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors  });

      })
    },
    private: false,
    validation: {
      email: Joi.string()
        .email()
        .required()
    }
  },

  /**
   * Registration of user.
   * @access public
   * @param {string} email
   * @param {string} firstname
   * @param {string} lastname
   * @param {string} password
   * @param {string} address
   */

  registerUser: {
    do: (data, response) => {
      models.user.registerUser(data).then(user => {
        return response({ isSuccess: true, data: data });

      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors });

      })
    },
    private: false,
    validation: {
      email: Joi.string()
        .email()
        .required(),
      firstname: Joi.string()
        .min(3)
        .max(30)
        .required(),
      lastname: Joi.string()
        .min(3)
        .max(30)
        .required(),
      password: Joi.string()
        .min(6)
        .max(30)
        .required(),
      address: Joi.string()
        .min(10)
        .max(300)
        .required(),
      is_free: Joi.boolean().required(),
      stripeToken: Joi.string().when("is_free", {
        is: false,
        then: Joi.required()
      })
    }
  },

  registerByInvite: {
    do: (data, response) => {
      models.invited_users.registerByInvite(data).then(user => {
        return response({ isSuccess: true, data: user });

      }).catch(errors => {
        return response({ isSuccess: false, data: errors });

      })
    },
    validation: {
      firstname: Joi.string()
        .min(3)
        .max(30)
        .required(),
      lastname: Joi.string()
        .min(3)
        .max(30)
        .required(),
      password: Joi.string()
        .min(6)
        .max(30)
        .required(),
      address: Joi.string()
        .min(10)
        .max(300)
        .required(),
      is_free: Joi.boolean().required(),
      stripeToken: Joi.string().when("is_free", {
        is: false,
        then: Joi.required()
      }),
      hash: Joi.string().required()
    }
  },

  /**
   * Login user.
   * @access public
   * @param {string} email
   * @param {string} password
   */

  login: {
    do: (data, response, socket) => {
      models.user.login(data).then(userInfo => {
        return response({ isSuccess: true, data: userInfo });

      })
      .catch(errors => {
        return response({ isSuccess: false, data:  errors });

      })
        
    },
    private: false,
    validation: {
      password: Joi.string().required(),
      email: Joi.string().required(),
      remember: Joi.boolean().required()
    }
  },

  /**
   * Email validation. Email address should be correct and uniq.
   * @access public
   * @param {string} email
   */

  checkEmail: {
    do: (data, response) => {
      models.user.checkEmail(data).then(() => {
        return response({ isSuccess: true });

      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors });

      })
    },
    private: false,
    validation: {
      email: Joi.string()
        .email()
        .required()
    }
  },

  /**
   * Confirmation email from link.
   * @access public
   * @param {string} hash - hash for email confirmation
   */

  confirmEmail: {
    do: (emailData, response) => {
      models.confirm_email.confirmEmail(emailData).then(() => {
        return response({ isSuccess: true });

      }).catch(errors => {
        return response({ isSuccess: false, data: errors });

      })

      
    }
  },
  private: false,
  validation: {
    hash: Joi.string().required()
  },

  /**
   * @access private
   * @param {string} stripetoken - payment token
   */
  upgradeUserAccount: {
    do: (data, response, socket, user) => {
      user.upgradeUserAccount(data, user).then(() => {
        return response({ isSuccess: true })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: null
  }
};
