const models = require("../models");
const Joi = require('joi');

module.exports = {

  getCurrentUser: {
    do: (data, response, socket, user) => {
      user.getFavoriteLists(data).then(lists => {
          return response({ isSuccess: true, data: { user, lists } })
      })
      
    },
    private: true
  },

  getUserCategories: {
    do: (data, response, socket, user) => {
      user.getCategories().then(categories => {
        return response({ isSuccess: true, data: categories })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true
  },

  /**
   * @access private
   * @param {string} firstname
   * @param {string} lastname
   * @param {string} address
   * @param {string} email
   */

  editUserInfo: {
    do: (data, response, socket, user) => {

      user.editUserInfo(data).then(user => {
        return response({ isSuccess: true, data: user });

      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors  });

      })
    },
    private: true,
    validation: {
      firstname: Joi.string(),
      lastname: Joi.string(),
      address: Joi.string(),
      email: Joi.string()
    }
  },

  /**
   * Send email for changing password.
   * @access private
   * @param {string} id - from token
   */

  changePasswordRequest: {
    do: (data, response, socket, user) => {
      models.reset_password.changePasswordRequest(data).then(() => {
        return response({ isSuccess: true });

      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors });

      });

    },
    private: false,
    validation: {
      email: Joi.string().required()
    }
  },

  /**
   * Changing password.
   * @access public
   * @param {string} hash - hash from link
   */

  changePassword: {
    do: (data, response) => {
      models.reset_password.changePassword(data).then(() => {
        return response({ isSuccess: true })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: false,
    validation: {
      hash: Joi.string().required(),
      password: Joi.string().required()
    }
  },
  /**
   * Changing password from user profile.
   * @access public
   * @param {string} current_password
   * @param {string} new_password
   */
  changePasswordFromProfile: {
    do: (data, response, socket, user) => {
      user.changePasswordFromProfile(data).then(() => {
        return response({ isSuccess: true })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      current_password: Joi.string().required(),
      new_password: Joi.string().required()
    }
  }
};
