const auth = require("./auth");
const user = require("./user");
const todoAssign = require("./todo-assign");
const todoLists = require("./todo-lists");
const todo = require('./todo')

module.exports = { ...auth, ...user, ...todoAssign, ...todoLists, ...todo };
