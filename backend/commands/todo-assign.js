const Joi = require("joi");
const models = require("../models");
const roomCommands = require('./roomCommands');

module.exports = {
  /**
   * @access private
   * @param {number} todo_list_id - todo list id
   * @param {string} email - email of assigned user
   */
  shareList: {
    do: (data, response, socket, user, io) => {
      user.shareList(data, user).then(list => {

          if(list.userForShareConnectionId) {
            io.sockets.sockets[list.userForShareConnectionId.socketId].join(list.id)    
          } 

          io.in(list.id).emit(roomCommands.listShared, list) 

          return response({ isSuccess: true, data: list })
          
      })
      .catch(errors => {
          return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      todo_list_id: Joi.number().required(),
      email: Joi.string().required()
    },
    broadcasting: true
    
  },
  /**
   * Assigned users for todo list
   * @access private
   * @param {number} todo_list_id - id of todo list 
   */
  getAssignedUsers: {
    do: (data, response, socket, user) => {
      models.todo_list.getAssignedUsers(data).then(users => {
        return response({ isSuccess: true, data: users })
      })
      .catch(errors => {
        return response({ isSuccess: false, data: errors })
      })
    },
    private: true,
    validation: {
      todo_list_id: Joi.number().required()
    }
  },
  
  /**
   * @access private
   * @param {string} email - substring for search
   */
  getUsersByEmail: {
      do: (data, response, socket, user) => {
          models.user.getUsersByEmail(data).then(users => {
            return response({ isSuccess: true, data: users })
          })
          .catch(errors => {
            return response({ isSuccess: false, data: errors })
          })
      },
      private: true,
      validation: {
          email: Joi.string().required()
      }
  }
};
