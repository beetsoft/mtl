(function() {
  "use strict";
  const models = require("../models");

  const userInfo = {
    email: "user1@mail.com",
    firstname: "user",
    lastname: "name",
    password: "password",
    address: "address",
    is_free: true
  };

  describe("Todo list actions", () => {
    let user;
    let list;
    let anotherUser;

    let anotherUserInfo = {
        email: "anotheremail@mail.com",
        firstname: "firstname",
        lastname: "lastname",
        address: "address",
        password: "password",
        is_active: true,
        is_free: false
    }
    
    beforeAll(done => {
      userInfo.is_active = true;
      return models.user
        .registerUser(Object.assign({}, userInfo))
        .then(newUser => {
          user = newUser;
          return models.user.create(anotherUserInfo).then(another => {
            anotherUser = another;
            console.log(anotherUser)
            done()
          })
        });
    });

    it("Create list", done => {
        return models.todo_list.createList({ title: "test" }, user).then(newList => {
            list = newList;
            expect(list.title).toBe("test")
            done()
        })
    });

    it("Edit list title", done => {
        return models.todo_list.editListTitle({ title: "new", id: list.id }, user).then(newList => {
            list = newList
            expect(newList.title).toBe("new")
            done()
        })
    })

    it("Edit list title with no permisson", done => {
        return models.todo_list.editListTitle({ title: "new", id: list.id }, anotherUser).catch(errors => {
            expect(errors.length).toBeGreaterThan(0);
            done();
        })
    })

    it("Return todo list", done => {
        return models.todo_list.getTodoList({ id: list.id }, user).then(result => {
            expect(result.id).toBe(list.id)
            done()
        })
    })

    it("Get todo list with no permission", done => {
        return models.todo_list.getTodoList({ id: list.id }, anotherUser).catch(errors => {
            expect(errors.length).toBeGreaterThan(0);
            done();
        })
    })

    it("Duplicate list", done => {
        return models.todo_list.duplicateList({ id: list.id }, user).then(newList => {
            expect(newList.title).toBe(list.title)
            return newList.destroy().then(() => {
              done()
            })
        })
    })

    it("DUplicate list with no permission", done => {
        return models.todo_list.duplicateList({ id: list.id }, anotherUser).catch(errors => {
            expect(errors.length).toBeGreaterThan(0);
            done();
        })
    })

    describe("Category actions", () => {
        let category;

        it("Create category with no title", done => {
            return models.todo_list.addCategoryToList({ todo_list_id: list.id}, anotherUser).then(() => {
                return models.todo_list.findOne({ where: { id: list.id } }).then(listWithCategory => {
                    expect(listWithCategory.category).toBeNull;
                    done()
                })
            })
        })

        it("Create category ", done => {
            return models.todo_list.addCategoryToList({ todo_list_id: list.id, title: "category" }, user).then(newCategory => {
                category = newCategory;
                expect(category.title).toBe("category");
                done()
            })
        })

        it("Edit category", done => {
            return models.todo_list.editCategory({ category_id: category.id, title: "new" }).then(newCategory => {
                expect(newCategory.title).toBe("new")
                category = newCategory;
                done()
            })
        })

        it("Delete category", done => {
            return models.todo_list.deleteCategory({ category_id: category.id }).then(() => {
                    
                return models.categories.findById(category.id).then(res => {
                    expect(res).toBeNull;
                    done()
                })
            })
        })
    })

    it("Must add todo list to favorits, then remove from it", done => {
        return models.todo_list.editFavoriteStatus({ id: list.id }, user).then(() => {
            return models.favorite_list.findOne({ where: { todo_list_id: list.id } }).then(favorite => {
                expect(favorite).not.toBeNull;

                return models.todo_list.editFavoriteStatus({ id: list.id }, user).then(() => {
                    return models.favorite_list.findOne({ where: { todo_list_id: list.id } }).then(favorite => { 
                        expect(favorite).toBeNull;
                        done()
                     })
                })
            })
        })
    })

    describe("Return favorite lists for current user", () => {
        beforeAll(done => {
            return models.todo_list.editFavoriteStatus({ id: list.id }, user).then(() => done())
        })

        it("Returns list", done => {
            user.getFavoriteLists().then(lists => {
                expect(lists.length).toBeGreaterThan(0)
                done()
            })
        })

        afterAll(done => {
            return models.todo_list.editFavoriteStatus({ id: list.id }, user).then(() => done())
        })
    })

    describe("Todo item actions", () => {
        let todoItem;

        it("Create new item", done => {
            return models.todo_list.addTodo({ todo_list_id: list.id, text: "test" }, user).then(todoList => {

                const newTodoItem = todoList.todo_items.filter(item => item.text === "test")[0]
    
                expect(newTodoItem.text).toBe("test")
    
                todoItem = newTodoItem 
                done()
            })
        })

        it("Edit item text", done => {
            return models.todo_list.editTodoText({ text: "new", id: todoItem.id }, user).then(todoListWithEditedItem => {

                const newTodoItem = todoListWithEditedItem.todo_items.filter(item => item.text === "new")[0]

                expect(newTodoItem.text).toBe("new")

                todoItem = newTodoItem 
                done()
            })
        })

        it("Edit item text with no permission", done => {
            return models.todo_list.editTodoText({ text: "new", id: todoItem.id }, anotherUser).catch(errors => {
                expect(errors.length).toBeGreaterThan(0);
                done();
            })
        })

        it("Edit item status", done => {
            return models.todo_list.editTodoStatus({ id: todoItem.id }, user).then(todoListWithEditedTodoStatus => {

                const newTodoItem = todoListWithEditedTodoStatus.todo_items.filter(item => item.id === todoItem.id )[0]

                expect(newTodoItem.is_done).not.toBe(todoItem.is_done)

                todoItem = newTodoItem 
                done()
            })
        })

        it("Edit item status with no permission", done => {
            return models.todo_list.editTodoStatus({ id: todoItem.id }, anotherUser).catch(errors => {
                expect(errors.length).toBeGreaterThan(0);
                done();
            })
        })

        it("Delete todo with no permission", done => {
            return models.todo_list.deleteTodo({ id: todoItem.id }, anotherUser).catch(errors => {
                expect(errors.length).toBeGreaterThan(0);
                done();
            })
        })

        it("Delete todo", done => {
            return models.todo_list.deleteTodo({ id: todoItem.id }, user).then(() => {

                return models.todo_item.findById(todoItem.id).then(result => {

                    expect(result).toBeNull;

                    done()
                })
            })
        })
    })


    it("Check owner of list", done => {
        return models.todo_list.checkOwner({ id: list.id, user_id: user.id }).then(result => {
            expect(result.id).toBe(list.id);
            done()
        })
    })

    it("Check owner with no permission", done => {
        return models.todo_list.checkOwner({ id: list.id, user_id: anotherUser.id }).catch(errors => {
            expect(errors.length).toBeGreaterThan(0);
            done();
        })
    })

    describe("Share actions", () => {
        let secondUser;
        let secondUserData = { 
            email: "email@mail.com", 
            password: "password", 
            address: "address", 
            firstname: "firstname", 
            lastname: "lastname"
        }

        beforeAll(done => {
            return models.user.create(secondUserData).then(newUser => {
                secondUser = newUser;
                return user.update({ is_free: false }).then(() => done());
            })
        })

        describe("Try to share as a free user", () => {
            let listForShare;
            let freeUser;
            let freeUserData = {
                email: "freeemail@mail.com",
                password: "password",
                address: "address",
                firstname: "firstname",
                lastname: "lastname",
                is_free: true
            }

            beforeAll(done => {
                return models.user.create(freeUserData).then(newUser => {
                    freeUser = newUser;
                    return models.todo_list.create({ user_id: freeUser.id, title: "title" }).then(list => {
                        listForShare = list;
                        done();
                    })
                })
            })

            it("Returns error", done => {
                return freeUser.shareList({ todo_list_id: listForShare.id, user_id: user.id }).catch(errors => {
                    expect(errors.length).toBeGreaterThan(0);
                    done();
                })
            })

            afterAll(done => {
                listForShare.destroy().then(() => freeUser.destroy().then(() => done()))
            })
        })

        it("Share list to existing user", done => {
            return user.shareList({ email: "email@mail.com", todo_list_id: list.id }, user).then(() => {
                return models.todo_assigned_users.findOne({ where: { user_id: secondUser.id, todo_list_id: list.id } }).then(assigned => {
                    expect(assigned).not.toBeNull;
                    done()
                })
            })
        })

        it("Share already shared list", done => {
            return user.shareList({ email: "email@mail.com", todo_list_id: list.id }).catch(errors => {
                expect(errors.length).toBeGreaterThan(0);
                done();
            })
        })

        it("Share list to yourself", done => {
            return user.shareList({ email: userInfo.email, todo_list_id: list.id }).catch(errors => {
                expect(errors.length).toBeGreaterThan(0);
                done();
            })
        })

        it("Return assigned users", done => {
            return models.todo_list.getAssignedUsers({ todo_list_id: list.id }).then(users => {
                expect(users.length).toBeGreaterThan(0);
                done()
            })
        })

        describe("Share and delete for non-existing user", () => {
            const email = "usernotexist33@mail.com"
            let invitedUser;

                it("Share list to non-existing user", done => {
                    return user.shareList({ email: email, todo_list_id: list.id }, user).then(() => {
                        return models.invited_users.findOne({ where: { email: email, todo_list_id: list.id } }).then(invited => {
                            expect(invited).not.toBeNull;
                            invitedUser = invited;
                            done()
                        })
                    })
                })

                it("Delete share for invited user and invited user", done => {
                    return models.todo_list.deleteShare({ user_id: invitedUser.id, todo_list_id: list.id, invited: true }).then(() => {
                        return models.invited_users.findOne({ where: { email: email, todo_list_id: list.id } }).then(result => {
                            expect(result).toBeNull;
                            done()
                        })
                    })
                    
                })
        })

        describe("Share multiple list to non-existing user", () => {
            let secondList;
            const invitedUserData = {
                email: "anothernotexistinguser2323@mail.com",
                firstname: "firstname",
                lastname: "lastname",
                address: "address",
                password: "password"
              }
            let invitedUser;
            
            beforeAll(done => {
                return models.todo_list.create({ user_id: user.id, title: "list" }).then(newList => {
                    secondList = newList;
                    done()
                })
            })

            it("Share two lists to user, get two records in DB, make sure they have same hash", done => {
                return user.shareList({ email: invitedUserData.email, todo_list_id: list.id }).then(() => {
                    return user.shareList({ email: invitedUserData.email, todo_list_id: secondList.id }).then(() => {
                        return models.invited_users.findAll({ where: { email: invitedUserData.email } }).then(sharedLists => {
                            expect(sharedLists.length).toBeGreaterThan(1);
                            expect(sharedLists[0].hash).toBe(sharedLists[1].hash);
                            done()
                        })
                    })
                })
            })

            it("Register invited user with two shared lists", done => {

                    return models.invited_users.findOne({ where: { email: invitedUserData.email, todo_list_id: list.id } }).then(request => {
                      invitedUserData.hash = request.hash;
                      invitedUserData.is_free = true;
            
                      return models.invited_users.registerByInvite(invitedUserData).then(newUser => {
                        invitedUser = newUser;
                        expect(newUser).not.toBeNull;
            
                        return models.todo_assigned_users.findAll({ where: { user_id: invitedUser.id } }).then(result => {
                          expect(result.length).toBeGreaterThan(1);
                          return models.todo_assigned_users.destroy({ where: { user_id: invitedUser.id } }).then(() => done())
                        })
                      })
                    })
            })

            afterAll(done => {
                invitedUser.destroy().then(() => secondList.destroy().then(() => done()))
            })
        })

        it("Delete share for user", done => {
            return models.todo_list.deleteShare({ todo_list_id: list.id, user_id: secondUser.id }, user).then(() => {
                return models.todo_assigned_users.findOne({ where: { user_id: secondUser.id, todo_list_id: list.id } }).then(assigned => {
                    expect(assigned).toBeNull;
                    done()
                })
            })
        })

        it("Delete share with no permisson", done => {
            return models.todo_list.deleteShare({ todo_list_id: list.id, user_id: user.id }, anotherUser).catch(errors => {
                expect(errors.length).toBeGreaterThan(0);
                done();
            })
        })

        afterAll(done => {
            secondUser.destroy().then(() => done())
        })

    })


    it("Delete list with no permisson", done => {
        return models.todo_list.deleteTodoList({id: list.id }, anotherUser).catch(errors => {
            expect(errors.length).toBeGreaterThan(0);
            done();
        })
    })

    it("Delete list", done => {
        return models.todo_list.deleteTodoList({ id: list.id }, user).then(() => {
            return models.todo_list.findById(list.id).then(result => {
                expect(result).toBeNull;
                    done()
            })
        })
    })

    afterAll(done => {
        return user.destroy().then(() => anotherUser.destroy().then(() => done()))
    })
  });
})();
