(function() {
  "use strict";
  const models = require("../models");

  const userInfo = {
        email: "user2@mail.com",
        firstname: "user",
        lastname: "name",
        password: "password",
        address: "address",
        is_free: true
      };

  describe("User actions", () => {
    let user;
    
    it("Must register free user", done => {
      models.user.registerUser(userInfo).then(newUser => {
        user = newUser;
        expect(userInfo.email).toEqual(newUser.email);
        done();
      });
    });

    describe("Registration by invite", () => {
      const invitedUserData = {
        firstname: "firstname",
        lastname: "lastname",
        address: "address",
        password: "password"
      }
      const invitedUserEmail = "emailisnotexist23@mail.com";
      let invitedUser;
      let list;

      beforeAll(done => {
        user.update({ is_free: false }).then(() => {
          return models.todo_list.create({ title: "text", user_id: user.id }).then(newList => {
            list = newList
            return user.shareList({ todo_list_id: newList.id, email: invitedUserEmail }).then(() => done())
          })
        })
      })

      it("Returns error for wrong hash", done => {
        invitedUserData.hash = "incorrect";
        models.invited_users.registerByInvite(invitedUserData).catch(errors => {
          expect(errors.length).toBeGreaterThan(0);
          done()
        })
      })

      it("User created and have access to todo list", done => {
        return models.invited_users.findOne({ where: { email: invitedUserEmail, todo_list_id: list.id } }).then(request => {
          invitedUserData.hash = request.hash;
          invitedUserData.is_free = true;

          return models.invited_users.registerByInvite(invitedUserData).then(newUser => {
            invitedUser = newUser;
            expect(newUser).not.toBeNull;

            return models.todo_assigned_users.findOne({ where: { todo_list_id: list.id, user_id: invitedUser.id } }).then(result => {
              expect(result).not.toBeNull;
              return result.destroy().then(() => {
                done()
              })
            })
          })
        })
        
      })

      afterAll(done => {
        invitedUser.destroy().then(() => list.destroy().then(() => done()))
      })
    })

    it("Login user with wrong email", done => {
      return models.user.login({ email: "not exist in db", password: "password" }).catch(errors => {
        expect(errors.length).toBeGreaterThan(0);
        done();
    })
    })

    it("Login inactive user", done => {
      return models.user.login({ email: userInfo.email, password: userInfo.password }).catch(errors => {
        expect(Array.isArray(errors)).toBe(true)
        done()
      })
    })

    it("Login active user", done => {
      return user.update({ is_active: true }).then(() => {
        return models.user.login({ email: userInfo.email, password: userInfo.password }).then(data => {
            expect(data.token.key).not.toBeNull;
            done()
        })
      })
      
    })

    it("Login with wrong password", done => {
      return models.user.login({ email: userInfo.email, password: "incorrect password" }).catch(errors => {
        expect(errors.length).toBeGreaterThan(0);
        done();
    })
    })

    it("Edit firstname", done => {
      return user.editUserInfo({ firstname: "Test" }).then(edited => {
        expect(edited.firstname).toBe("Test")
        done()
      })
    })

    it("Search users by email", done => {
      return models.user.getUsersByEmail({ email: "us" }).then(users => {
        expect(users.filter(user => user.email === userInfo.email).length > 0).toBe(true);
        done()
      })
    })

    it("Change user password from profile", done => {
      return user.changePasswordFromProfile({ current_password: userInfo.password, new_password: "new" }).then(() => {
        userInfo.password = "new"
        return models.user.login({ email: userInfo.email, password: userInfo.password }).then(data => {
          expect(data.token.key).not.toBeNull;
          done()
        })
      })
    })

    it("Change password from profile with wrong current password", done => {
      return user.changePasswordFromProfile({ current_password:  "wrong password", new_password: "new" }).catch(errors => {
        expect(errors.length).toBeGreaterThan(0);
        done();
    })
    })


    describe("Get assigned anw own todo lists of user", () => {
      let secondUser;
      let list;
      let sharedList;
      let secondUserData = {
        email: "randomemail@mail.com",
        address: "address",
        firstname: "firstanme",
        lastname: "lastname",
        password: "password",
        is_free: false,
        is_active: true
      }

      beforeAll(done => {
        return models.user.create(secondUserData).then(newUser => {
          secondUser = newUser;
          return models.todo_list.create({ title: "own", user_id: user.id }).then(ownList => {
            list = ownList;
            return models.todo_list.create({ title: "shared", user_id: secondUser.id }).then(newSharedList => {
              sharedList = newSharedList;
              return models.todo_assigned_users.create({ user_id: user.id, todo_list_id: sharedList.id }).then(() => done())
            })
          })
        })
      })

      it("Return todo lists", done => {
        return user.getAllTodoLists().then(lists => {
          expect(lists.length).toBeGreaterThan(0);
          done()
        })
      })

      afterAll(done => {
          return sharedList.destroy().then(() => {
            return list.destroy().then(() => {
              return secondUser.destroy().then(() => done())
          })
        })
      })

    })


    describe("Check email or exist", () => {
      let user;
      let userData = {
        email: "address@mail.com",
        firstname: "firstname",
        lastname: "lastname",
        password: "password"
      }

      beforeAll(done => {
        return models.user.create(userData).then(newUser => {
          user = newUser
          done()
        })
      })

      it("Check email for existing in DB", done => {
        return models.user.checkEmail({ email: userData.email} ).catch(error => {
          expect(Array.isArray(error)).toBe(true)
          return user.destroy().then(() => {
            return models.user.checkEmail(userData.email).then(res => {
              expect(res).toBe(true)
              done()
            })
          })
        })
      })
    })

    afterAll(done => {
      user.destroy().then(() => done())
    })

  });

})();
