(function() {
    "use strict";
    const models = require("../models");
  
    const userInfo = {
          email: "user@mail.com",
          firstname: "user",
          lastname: "name",
          password: "password",
          address: "address",
          is_free: true,
          is_active: true
        };

    let user;
    let request;

    beforeAll(done => {
        return models.user.registerUser(userInfo).then(newUser => {
            user = newUser;
            done()
        })
    })

    it("Create request for change password", done => {
        return models.reset_password.changePasswordRequest({ email: userInfo.email }).then(() => {
            return models.reset_password.findOne({ where: { email: userInfo.email } }).then(newRequest => {
                request = newRequest;
                expect(request).not.toBeNull;
                done()
            })
        })
    })

    it("Trying to create request for user who doesn't exist, must return error", done => {
        return models.reset_password.changePasswordRequest({ email: "wrongemail" }).catch(errors => {
            expect(errors.length).toBeGreaterThan(0);
            done()
        })
    })

    it("Change password", done => {
        return models.reset_password.changePassword({ hash: request.hash, password: "new" }).then(() => {
            return models.user.login({ email: userInfo.email, password: "new" }).then(data => {
                expect(data.token.key).not.toBeNull;
                done()
            })
        })
    })

    it("Change password with invalid link", done => {
        return models.reset_password.changePassword({ hash: "incorrect hash", password: "new" }).catch(errors => {
            expect(errors.length).toBeGreaterThan(0);
            done()
        })
    })

    afterAll(done => {
        user.destroy().then(() => done())
    })

})()