(function() {
    'use strict';

    const models = require('../models');

    const userData = {
        email: 'user3@mail.com', 
        firstname: 'user', 
        lastname: 'name',
        password: 'password', 
        address: 'address'
    }

    describe('Must create request for email confirmation', () => {
        let user;
        let requestData;
            
        beforeAll((done) => {
            return models.user.create(userData).then(newUser => {
                user = newUser;
                return done();
            });
        });

        it('Create request', (done) => {
            return models.confirm_email.confirmEmailRequest({user_id: user.id, email: user.email}).then(data => {
                requestData = data
                return models.confirm_email.findOne({where: {email: user.email}}).then(request => {
                    expect(request.email).toBe(user.email);
                    return done();
                });
            });
        });

        it("Resend request if it already exist", done => {
            return models.confirm_email.confirmEmailRequest({user_id: user.id, email: user.email}).then(data => {
                requestData = data
                return models.confirm_email.findOne({where: {email: user.email}}).then(request => {
                    expect(request.email).toBe(user.email);
                    return done();
                });
            });
        })

        

        describe("Change user status and delete request from DB", () => {

            it("Confirm email with wrong link", done => {
                return models.confirm_email.confirmEmail({ hash: "wrong hash" }).catch(errors => {
                    expect(errors.length).toBeGreaterThan(0)
                    done()
                })
            })

            it("Change status", done => {
                return models.confirm_email.confirmEmail({ hash: requestData.hash }).then(data => {
                    return models.confirm_email.findOne({ where: { email: user.email } }).then(request => {
                        
                        expect(request).toBeNull;
                        expect(data.user.is_active).toBe(true)
                        return done()
                    })
                })
            })
        })

        afterAll((done) => {
            return user.destroy().then(() => done())
        })

    });
}());