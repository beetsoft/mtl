'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "invited_users", deps: []
 *
 **/

var info = {
    "revision": 2,
    "name": "invite",
    "created": "2019-01-17T09:41:32.983Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "createTable",
    params: [
        "invited_users",
        {
            "id": {
                "type": Sequelize.INTEGER,
                "field": "id",
                "primaryKey": true,
                "autoIncrement": true
            },
            "hash": {
                "type": Sequelize.STRING,
                "field": "hash",
                "allowNull": false
            },
            "email": {
                "type": Sequelize.STRING,
                "field": "email",
                "allowNull": false,
                "unique": false
            },
            "inviteTodo": {
                "type": Sequelize.INTEGER,
                "field": "inviteTodo",
                "allowNull": false
            },
            "createdAt": {
                "type": Sequelize.DATE,
                "field": "createdAt",
                "allowNull": false
            },
            "updatedAt": {
                "type": Sequelize.DATE,
                "field": "updatedAt",
                "allowNull": false
            }
        },
        {
            "charset": "utf8"
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
