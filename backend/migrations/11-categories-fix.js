'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "title" on table "categories"
 *
 **/

var info = {
    "revision": 11,
    "name": "categories-fix",
    "created": "2019-02-18T10:03:00.152Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "changeColumn",
    params: [
        "categories",
        "title",
        {
            "type": Sequelize.STRING,
            "field": "title",
            "allowNull": false,
            "uniq": false
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
