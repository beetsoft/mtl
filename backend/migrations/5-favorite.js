'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "favorite_lists", deps: [users, todo_lists]
 *
 **/

var info = {
    "revision": 5,
    "name": "favorite",
    "created": "2019-02-08T07:53:36.171Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "createTable",
    params: [
        "favorite_lists",
        {
            "id": {
                "type": Sequelize.INTEGER,
                "field": "id",
                "primaryKey": true,
                "autoIncrement": true
            },
            "created_at": {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            },
            "updated_at": {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            },
            "user_id": {
                "type": Sequelize.INTEGER,
                "field": "user_id",
                "onUpdate": "CASCADE",
                "onDelete": "SET NULL",
                "references": {
                    "model": "users",
                    "key": "id"
                },
                "allowNull": true
            },
            "todo_list_id": {
                "type": Sequelize.INTEGER,
                "field": "todo_list_id",
                "onUpdate": "CASCADE",
                "onDelete": "SET NULL",
                "references": {
                    "model": "todo_lists",
                    "key": "id"
                },
                "allowNull": true
            }
        },
        {
            "charset": "utf8"
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
