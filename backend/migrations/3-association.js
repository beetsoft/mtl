'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "createdAt" from table "reset_passwords"
 * removeColumn "updatedAt" from table "todo_assigned_users"
 * removeColumn "createdAt" from table "users"
 * removeColumn "updatedAt" from table "users"
 * removeColumn "createdAt" from table "todo_assigned_users"
 * removeColumn "todolist_id" from table "todo_assigned_users"
 * removeColumn "author_id" from table "todo_lists"
 * removeColumn "createdAt" from table "todo_lists"
 * removeColumn "updatedAt" from table "todo_lists"
 * removeColumn "updatedAt" from table "confirm_emails"
 * removeColumn "createdAt" from table "confirm_emails"
 * removeColumn "todolist_id" from table "todo_items"
 * removeColumn "createdAt" from table "todo_items"
 * removeColumn "updatedAt" from table "todo_items"
 * removeColumn "updatedAt" from table "invited_users"
 * removeColumn "createdAt" from table "invited_users"
 * removeColumn "inviteTodo" from table "invited_users"
 * removeColumn "updatedAt" from table "reset_passwords"
 * createTable "payment_data", deps: [users]
 * addColumn "created_at" to table "users"
 * addColumn "updated_at" to table "users"
 * addColumn "user_id" to table "todo_lists"
 * addColumn "updated_at" to table "todo_lists"
 * addColumn "updated_at" to table "reset_passwords"
 * addColumn "created_at" to table "reset_passwords"
 * addColumn "created_at" to table "confirm_emails"
 * addColumn "updated_at" to table "confirm_emails"
 * addColumn "user_id" to table "reset_passwords"
 * addColumn "created_at" to table "todo_lists"
 * addColumn "created_at" to table "invited_users"
 * addColumn "todo_list_id" to table "todo_items"
 * addColumn "created_at" to table "todo_assigned_users"
 * addColumn "updated_at" to table "invited_users"
 * addColumn "updated_at" to table "todo_assigned_users"
 * addColumn "updated_at" to table "todo_items"
 * addColumn "todo_list_id" to table "invited_users"
 * addColumn "todo_list_id" to table "todo_assigned_users"
 * addColumn "user_id" to table "confirm_emails"
 * addColumn "created_at" to table "todo_items"
 * changeColumn "user_id" on table "todo_assigned_users"
 *
 **/

var info = {
    "revision": 3,
    "name": "association",
    "created": "2019-01-22T10:24:08.499Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "removeColumn",
        params: ["reset_passwords", "createdAt"]
    },
    {
        fn: "removeColumn",
        params: ["todo_assigned_users", "updatedAt"]
    },
    {
        fn: "removeColumn",
        params: ["users", "createdAt"]
    },
    {
        fn: "removeColumn",
        params: ["users", "updatedAt"]
    },
    {
        fn: "removeColumn",
        params: ["todo_assigned_users", "createdAt"]
    },
    {
        fn: "removeColumn",
        params: ["todo_assigned_users", "todolist_id"]
    },
    {
        fn: "removeColumn",
        params: ["todo_lists", "author_id"]
    },
    {
        fn: "removeColumn",
        params: ["todo_lists", "createdAt"]
    },
    {
        fn: "removeColumn",
        params: ["todo_lists", "updatedAt"]
    },
    {
        fn: "removeColumn",
        params: ["confirm_emails", "updatedAt"]
    },
    {
        fn: "removeColumn",
        params: ["confirm_emails", "createdAt"]
    },
    {
        fn: "removeColumn",
        params: ["todo_items", "todolist_id"]
    },
    {
        fn: "removeColumn",
        params: ["todo_items", "createdAt"]
    },
    {
        fn: "removeColumn",
        params: ["todo_items", "updatedAt"]
    },
    {
        fn: "removeColumn",
        params: ["invited_users", "updatedAt"]
    },
    {
        fn: "removeColumn",
        params: ["invited_users", "createdAt"]
    },
    {
        fn: "removeColumn",
        params: ["invited_users", "inviteTodo"]
    },
    {
        fn: "removeColumn",
        params: ["reset_passwords", "updatedAt"]
    },
    {
        fn: "createTable",
        params: [
            "payment_data",
            {
                "id": {
                    "type": Sequelize.INTEGER,
                    "field": "id",
                    "primaryKey": true,
                    "autoIncrement": true
                },
                "token": {
                    "type": Sequelize.STRING,
                    "field": "token",
                    "allowNull": false
                },
                "amount": {
                    "type": Sequelize.STRING,
                    "field": "amount",
                    "allowNull": false
                },
                "currensy": {
                    "type": Sequelize.STRING,
                    "field": "currensy",
                    "allowNull": false
                },
                "receipt_url": {
                    "type": Sequelize.STRING,
                    "field": "receipt_url",
                    "allowNull": false
                },
                "paid": {
                    "type": Sequelize.STRING,
                    "field": "paid",
                    "allowNull": false
                },
                "created": {
                    "type": Sequelize.STRING,
                    "field": "created",
                    "allowNull": false
                },
                "created_at": {
                    "type": Sequelize.DATE,
                    "field": "created_at",
                    "allowNull": false
                },
                "updated_at": {
                    "type": Sequelize.DATE,
                    "field": "updated_at",
                    "allowNull": false
                },
                "user_id": {
                    "type": Sequelize.INTEGER,
                    "field": "user_id",
                    "onUpdate": "CASCADE",
                    "onDelete": "SET NULL",
                    "references": {
                        "model": "users",
                        "key": "id"
                    },
                    "allowNull": true
                }
            },
            {
                "charset": "utf8"
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "users",
            "created_at",
            {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "users",
            "updated_at",
            {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_lists",
            "user_id",
            {
                "type": Sequelize.INTEGER,
                "field": "user_id",
                "onUpdate": "CASCADE",
                "onDelete": "SET NULL",
                "references": {
                    "model": "users",
                    "key": "id"
                },
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_lists",
            "updated_at",
            {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "reset_passwords",
            "updated_at",
            {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "reset_passwords",
            "created_at",
            {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "confirm_emails",
            "created_at",
            {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "confirm_emails",
            "updated_at",
            {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "reset_passwords",
            "user_id",
            {
                "type": Sequelize.INTEGER,
                "field": "user_id",
                "onUpdate": "CASCADE",
                "onDelete": "SET NULL",
                "references": {
                    "model": "users",
                    "key": "id"
                },
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_lists",
            "created_at",
            {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "invited_users",
            "created_at",
            {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_items",
            "todo_list_id",
            {
                "type": Sequelize.INTEGER,
                "field": "todo_list_id",
                "onUpdate": "CASCADE",
                "onDelete": "SET NULL",
                "references": {
                    "model": "todo_lists",
                    "key": "id"
                },
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_assigned_users",
            "created_at",
            {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "invited_users",
            "updated_at",
            {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_assigned_users",
            "updated_at",
            {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_items",
            "updated_at",
            {
                "type": Sequelize.DATE,
                "field": "updated_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "invited_users",
            "todo_list_id",
            {
                "type": Sequelize.INTEGER,
                "field": "todo_list_id",
                "onUpdate": "CASCADE",
                "onDelete": "SET NULL",
                "references": {
                    "model": "todo_lists",
                    "key": "id"
                },
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_assigned_users",
            "todo_list_id",
            {
                "type": Sequelize.INTEGER,
                "field": "todo_list_id",
                "onUpdate": "CASCADE",
                "onDelete": "SET NULL",
                "references": {
                    "model": "todo_lists",
                    "key": "id"
                },
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "todo_items",
            "created_at",
            {
                "type": Sequelize.DATE,
                "field": "created_at",
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "todo_assigned_users",
            "user_id",
            {
                "type": Sequelize.INTEGER,
                "onUpdate": "CASCADE",
                "onDelete": "CASCADE",
                "references": {
                    "model": "users",
                    "key": "id"
                },
                "field": "user_id",
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
