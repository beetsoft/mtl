'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "email" on table "invited_users"
 *
 **/

var info = {
    "revision": 12,
    "name": "invite-fix",
    "created": "2019-02-19T06:57:29.573Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "changeColumn",
    params: [
        "invited_users",
        "email",
        {
            "type": Sequelize.STRING,
            "field": "email",
            "allowNull": false,
            "unique": false
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
