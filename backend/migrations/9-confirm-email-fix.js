'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "user_id" on table "confirm_emails"
 *
 **/

var info = {
    "revision": 9,
    "name": "confirm-email-fix",
    "created": "2019-02-14T05:35:53.735Z",
    "comment": ""
};

var migrationCommands = [
    {
        fn: "addColumn",
        params: [
            "confirm_emails",
            "user_id",
            {
                "type": Sequelize.INTEGER,
                "field": "user_id",
                "onUpdate": "CASCADE",
                "onDelete": "cascade",
                "references": {
                    "model": "users",
                    "key": "id"
                },
                "allowNull": true
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
