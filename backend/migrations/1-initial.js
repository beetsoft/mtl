'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "todo_assigned_users", deps: []
 * createTable "confirm_emails", deps: []
 * createTable "reset_passwords", deps: []
 * createTable "todo_items", deps: []
 * createTable "todo_lists", deps: []
 * createTable "users", deps: []
 *
 **/

var info = {
    "revision": 1,
    "name": "initial",
    "created": "2019-01-17T09:40:58.329Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "createTable",
        params: [
            "todo_assigned_users",
            {
                "id": {
                    "type": Sequelize.INTEGER,
                    "field": "id",
                    "primaryKey": true,
                    "autoIncrement": true
                },
                "todolist_id": {
                    "type": Sequelize.INTEGER,
                    "field": "todolist_id",
                    "allowNull": false
                },
                "user_id": {
                    "type": Sequelize.INTEGER,
                    "field": "user_id",
                    "allowNull": false
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            },
            {
                "charset": "utf8"
            }
        ]
    },
    {
        fn: "createTable",
        params: [
            "confirm_emails",
            {
                "id": {
                    "type": Sequelize.INTEGER,
                    "field": "id",
                    "primaryKey": true,
                    "autoIncrement": true
                },
                "email": {
                    "type": Sequelize.STRING,
                    "field": "email",
                    "allowNull": false,
                    "unique": true
                },
                "hash": {
                    "type": Sequelize.STRING,
                    "field": "hash",
                    "allowNull": false
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            },
            {
                "charset": "utf8"
            }
        ]
    },
    {
        fn: "createTable",
        params: [
            "reset_passwords",
            {
                "id": {
                    "type": Sequelize.INTEGER,
                    "field": "id",
                    "primaryKey": true,
                    "autoIncrement": true
                },
                "email": {
                    "type": Sequelize.STRING,
                    "field": "email",
                    "allowNull": false,
                    "unique": true
                },
                "hash": {
                    "type": Sequelize.STRING,
                    "field": "hash",
                    "allowNull": false,
                    "unique": true
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            },
            {
                "charset": "utf8"
            }
        ]
    },
    {
        fn: "createTable",
        params: [
            "todo_items",
            {
                "id": {
                    "type": Sequelize.INTEGER,
                    "field": "id",
                    "primaryKey": true,
                    "autoIncrement": true
                },
                "text": {
                    "type": Sequelize.TEXT,
                    "field": "text",
                    "allowNull": false
                },
                "is_done": {
                    "type": Sequelize.BOOLEAN,
                    "field": "is_done",
                    "defaultValue": false
                },
                "todolist_id": {
                    "type": Sequelize.INTEGER,
                    "field": "todolist_id",
                    "allowNull": false
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            },
            {
                "charset": "utf8"
            }
        ]
    },
    {
        fn: "createTable",
        params: [
            "todo_lists",
            {
                "id": {
                    "type": Sequelize.INTEGER,
                    "field": "id",
                    "primaryKey": true,
                    "autoIncrement": true
                },
                "author_id": {
                    "type": Sequelize.INTEGER,
                    "field": "author_id",
                    "allowNull": false
                },
                "title": {
                    "type": Sequelize.TEXT,
                    "field": "title"
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            },
            {
                "charset": "utf8"
            }
        ]
    },
    {
        fn: "createTable",
        params: [
            "users",
            {
                "id": {
                    "type": Sequelize.INTEGER,
                    "field": "id",
                    "primaryKey": true,
                    "autoIncrement": true
                },
                "email": {
                    "type": Sequelize.STRING,
                    "field": "email",
                    "allowNull": false,
                    "unique": true
                },
                "firstname": {
                    "type": Sequelize.STRING,
                    "field": "firstname"
                },
                "lastname": {
                    "type": Sequelize.STRING,
                    "field": "lastname"
                },
                "password": {
                    "type": Sequelize.STRING,
                    "field": "password",
                    "allowNull": false
                },
                "is_free": {
                    "type": Sequelize.BOOLEAN,
                    "field": "is_free",
                    "defaultValue": true
                },
                "is_active": {
                    "type": Sequelize.BOOLEAN,
                    "field": "is_active",
                    "defaultValue": false
                },
                "address": {
                    "type": Sequelize.TEXT,
                    "field": "address"
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            },
            {
                "charset": "utf8"
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
