'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "color" to table "invited_users"
 *
 **/

var info = {
    "revision": 8,
    "name": "invited-color",
    "created": "2019-02-12T10:08:11.479Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "addColumn",
    params: [
        "invited_users",
        "color",
        {
            "type": Sequelize.STRING,
            "field": "color"
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
