# Application Multi-Todo List


## Development environment with Docker

**1. Install Docker + Docker Compose (https://docs.docker.com/compose)**

**2. Get project:**

```sh 
$ git clone <url-to-repo>
```

**3. Run the docker containers:**

```sh
$ cd /path/to/project
$ docker-compose --file docker-compose.yml --file docker-compose.local.yml up
```



**4. PhpMyAdmin available by url http://127.0.0.1:8080**

**5. Go to http://127.0.0.1 and enjoy**


##Migrations:##

**Create migration**
```sh
$ cd /path/to/project/backend
$ node ./node_modules/sequelize-auto-migrations/bin/makemigration --name <migration name>
```
**Run migration**

```sh
$ cd /path/to/project/backend
$ node ./node_modules/sequelize-auto-migrations/bin/runmigration --rev <version of migration>
```

