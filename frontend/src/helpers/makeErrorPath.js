/**
* Returns Object with path and name of error.
* @param {Object} error - Object of error.
* @return {Object} Returns Object with path and name of error.
*/
export function makeErrorPath(errors) {
  const errorsData = errors.error ? errors.error.details : errors;
  return errorsData.map(error => {
    if(error.label){
      return {label: error.label, path: error.path};
    }
    const type = error.type;
    const label = error.context.label;
    let path = 'errors.' + label + ".type." + type;

    return {label: label, path: path};
  })
}
