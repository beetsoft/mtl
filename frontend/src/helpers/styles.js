export const modalWindow = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    minHeight             : '200px',
    borderRadius          : '0',
    padding               : '0',
  },

  overlay: {
    backgroundColor       : 'rgba(0, 0, 0, 0.25)',
    border                : 'none',
  }
};

export const roundedModalWindow = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    minHeight             : '200px',
    borderRadius          : '5px',
    boxShadow             : '0 2px 6px rgba(0, 0, 0, 0.37)',
    padding               : '0',
  },

  overlay: {
    backgroundColor       : 'rgba(0, 0, 0, 0.25)',
    border                : 'none',
  }
};

export const checklistModalWindow = {
  content : {
    top                   : '187px',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, 0)',
    borderRadius          : '5px',
    boxShadow             : '0 2px 6px rgba(0, 0, 0, 0.37)',
    padding               : '0',
    overflow              : 'visible'
  },

  overlay: {
    backgroundColor       : 'rgba(0, 0, 0, 0.25)',
    border                : 'none',
  }
};

export const menuStyle = {
  borderRadius: '3px',
  boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
  background: 'rgba(255, 255, 255, 0.9)',
  padding: '2px 0',
  fontSize: '90%',
  position: 'fixed',
  left: '20px',
  top: '140px',
  overflow: 'auto',
  maxHeight: '50%',
};

export function randomColor() {
  const borderColors = [
    '#C82376',
    '#F5A623',
    '#DA483A'
  ];
  return '2px solid ' + borderColors[Math.floor(Math.random() * borderColors.length)];
}

export const stepsClassNames = "hiddenStep";
