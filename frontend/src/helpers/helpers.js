/**
* Cut the message to the required length.
* @param {String} fullMessage - Full message.
*/
export function cutMessage(fullMessage){
  let message = fullMessage.slice(0,15);
  if (message.length < fullMessage.length) {
    message += '...';
  }
  return message;
}

export function sharedList(users, quantity) {
  return users && users.length > quantity ? true : false;
}
