import { assignedUserConstants } from '../constants/assignedUserConstants';
import { fromJS, List } from 'immutable';

const initState = fromJS({
  users: [],
  assignedUsers: []
})

export function assignedUsers(state = initState, action) {
  switch (action.type) {
    case assignedUserConstants.GET_USERS:
      return state.merge({users: List(action.data)});
    case assignedUserConstants.GET_ASSIGNED_USERS:
      return state.merge({assignedUsers: List(action.data)});
    case assignedUserConstants.CLEAR_ASSIGNED_USERS:
      return state.merge({users: List([])});
    default:
      return state;
  }
}
