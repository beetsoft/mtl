import {toDoConstants} from '../constants';
import { fromJS } from 'immutable';

const initState = fromJS({
  currentToDo: {}
})

export function toDo(state = initState, action) {
  switch (action.type) {
    case toDoConstants.ADD_TO_DO:
      return state;
    default:
      return state;
  }
}
