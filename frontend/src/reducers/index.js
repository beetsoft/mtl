import { combineReducers } from 'redux';
import { user } from './user.reducer';
import { alert } from './alert.reducer'
import { toDo } from './toDo.reducer';
import { toDoList } from './toDoList.reducer';
import { assignedUsers } from './assignedUser.reducer';
import { categories } from './categories.reducer';

const rootReducer = combineReducers({
  user,
  alert,
  toDo,
  toDoList,
  assignedUsers,
  categories
});
export default rootReducer;
