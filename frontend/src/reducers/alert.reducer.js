import { alertConstants } from '../constants/alertConstants';

/** @function alert
* Set alert and it message about success or error in store.
*/
export function alert(state = {}, action) {
  switch (action.type) {
    case alertConstants.SUCCESS:
      return {
        type: 'alert-success',
        message: action.message
      };
    case alertConstants.ERROR:
      return Object.assign({}, state, {
        type: 'alert-danger',
        message: action.message
      });
    case alertConstants.CLEAR:
      return {};
    default:
      return state
  }
}
