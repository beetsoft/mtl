import { userConstants } from '../constants';
import { fromJS, Map, List } from 'immutable';

const initState = fromJS({
  data: {
    email: '',
    firstname: '',
    lastname: '',
    password: '',
    confirmPassword: '',
    is_free: true
  },
  address: '',
  token: {},
  registrationStep: 1,
  langs: ['ru', 'en'],
  favoriteLists: []
});

export function user(state = initState, action){
  switch (action.type) {
    case userConstants.DATA:
      const data = state.get('data');
      const lists = state.get('favoriteLists');
      let user = action.data;
      let favLists = [];
      if(action.data.user){
        user = action.data.user;
        favLists = action.data.lists;
      }
      const fav = favLists.map(e => {return e.todo_list_id});
      return state.merge({data: data.merge(user), favoriteLists: lists.merge(fav)});
    case userConstants.ADDRESS:
      return state.merge({address: action.address});
    case userConstants.REGISTRATION_STEP:
      return state.merge({registrationStep: action.step});
    case userConstants.CLEAR:
      return state.merge(initState);
    case userConstants.SET_TOKEN:
      const token = state.get('token');
      return state.merge({token: token.merge(action.token)});
    case userConstants.REMOVE_TOKEN:
      return state.merge({token: Map({})});
    case userConstants.CHANGE_FAVORITE:
      let favoriteLists = state.get('favoriteLists').toJS();
      const favoriteIndex = favoriteLists.indexOf(action.id)
      if(favoriteIndex < 0){
        favoriteLists.push(action.id);
      } else {
        favoriteLists.splice([favoriteIndex], 1);
      }
      return state.merge({favoriteLists: List(favoriteLists)});
    default:
      return state;
  }
}
