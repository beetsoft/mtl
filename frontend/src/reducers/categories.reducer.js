import {categoriesConstants} from '../constants';
import { fromJS, Map, List } from 'immutable';

const initState = fromJS({
  categories: []
})

export function categories(state = initState, action) {
  switch (action.type) {
    case categoriesConstants.CREATE_CATEGORY:
      let categoriesList = state.get('categories');
      if(categoriesList.toJS().map(function(e) { return e.id; }).indexOf(action.data.id) >= 0){
        return state;
      }
      categoriesList = categoriesList.push(action.data);
      return state.merge({categories: List(categoriesList)});
    case categoriesConstants.EDIT_CATEGORY:
      return state;
    case categoriesConstants.DELETE_CATEGORY:
      const deletedCategory = state.get('categories').toJS().filter(o => o.id !== action.id)
      return state.merge({categories: List(deletedCategory)});
    case categoriesConstants.SET_CATEGORIES:
      return state.merge({categories: List(action.data)});
    case categoriesConstants.CHANGE_CATEGORY_TITLE:
      let categories = state.get('categories');
      categories = categories.update(
        categories.findIndex( category => {
          return Map(category).get("id") === action.data.id;
        }), category => {
          return Map(category).merge(action.data);
        }
      );
      return state.merge({categories: categories});
    default:
      return state;
  }
}
