import {toDoListConstants} from '../constants';
import { fromJS, Map, List } from 'immutable';

const initState = fromJS({
  currentToDoList: {
    title: '',
    newToDo: ''
  },
  toDoLists: []
})

export function toDoList(state = initState, action) {
  switch (action.type) {
    case toDoListConstants.GET_TO_DO_LIST:
      const currentToDoList = state.get('currentToDoList');
      return state.merge({currentToDoList: currentToDoList.merge(action.data)});
    case toDoListConstants.GET_TO_DO_LISTS:
      return state.merge({toDoLists: List(action.data)});
    case toDoListConstants.CREATE_LIST:
      let createdLists = state.get('toDoLists').push(action.data);
      return state.merge({toDoLists: List(createdLists)});
    case toDoListConstants.UPDATE_LIST:
      let lists = state.get('toDoLists');
      lists = lists.update(
        lists.findIndex( list => {
          return Map(list).get("id") === action.data.id;
        }), list => {
          return Map(list).merge(action.data);
        }
      );
      return state.merge({toDoLists: lists});
    case toDoListConstants.DELETE_SHARE:
      const users = action.data.invited ? 'invited_users' : 'assigned_users';
      let toDoLists = state.get('toDoLists');
      toDoLists = toDoLists.update(
        toDoLists.findIndex( list => {
          return Map(list).get("id") === action.data.todo_list_id;
        }), list => {
          list = Map(list);
          return list.set(users, list.get(users).filter(user => user.id !== action.data.userId));
        }
      );
      return state.merge({toDoLists: toDoLists});
    case toDoListConstants.DELETE_LIST:
      const newLists = state.get('toDoLists').toJS().filter(o => o.id !== action.id)
      return state.merge({toDoLists: List(newLists)});
    case toDoListConstants.CLEAR_TO_DO_LIST:
      return state.merge({currentToDoList: Map({title: ''})});
    default:
      return state;
  }
}
