import React, {Component} from 'react';
import './assets/styles/App.scss';
import Header from './components/header';
import ThanksPage from './components/helperPages/thanksPage';
import Login from './components/login';
import ErrorPage from './components/helperPages/errorPage';
import {connect} from 'react-redux';
import './components/helperComponents/i18n';
import {Route, Switch} from 'react-router-dom';
import ConfirmEmail from './components/helperPages/confirmEmail';
import PrivateRoute from './components/helperComponents/privateRoute';
import Dashboard from './components/dashboard';
import UserProfile from "./components/static/UserProfile/UserProfile";
import UserChecklists from "./components/static/UserChecklists/UserChecklists";
import Invite from './components/invite';

class App extends Component {

    render() {
        return (
            <div className="App">
                <div className="App__header">
                    <Header/>
                </div>
                <div className="App__content">
                    <Switch>
                        <Route exact path="/" component={Login}/>
                        <PrivateRoute path="/dashboard" component={Dashboard}/>
                        <Route path="/thanksPage" component={ThanksPage}/>
                        <Route path="/confirm_email" component={ConfirmEmail}/>
                        <PrivateRoute path="/user-profile"  component={UserProfile}/>
                        <Route path="/user-checklists"  component={UserChecklists}/>
                        <Route path="/invite" component={Invite} />

                        <Route component={ErrorPage}/>
                    </Switch>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
  const { registrationStep } = state.user;
  return { registrationStep };
}

export default connect(mapStateToProps)(App);
