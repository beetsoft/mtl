import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { userActions } from '../actions/user.actions';
import { withNamespaces } from "react-i18next";
import Modal from 'react-modal';
import {modalWindow} from '../helpers/styles';
import Registration from './registration/registration';
import RecoverPassword from './helperPages/recoverPassword';
import {Redirect} from 'react-router-dom';
import '../assets/styles/login.style.scss';
import Input from './helperComponents/input';
import { Link } from 'react-router-dom';
import * as Joi from 'joi-browser';
import { makeErrorPath } from '../helpers/makeErrorPath';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: {
              email: '',
              password: '',
              remember: false
            },
            errors: {
                email: '',
                password: '',
            },
            modalIsOpen: false,
            recoverWindowIsOpen: false,
            showDummyError: false,
            showSuccessSend: false,
            hash: null
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.openRecoverWindow = this.openRecoverWindow.bind(this);
    }

    /**
     * Open modal window for registration.
     */
    openModal() {
        this.setState({modalIsOpen: true});
    }

    /**
     * Open modal window for recover password.
     */
    openRecoverWindow(hash) {
        this.setState({recoverWindowIsOpen: true, hash: hash});
    }

    /**
     * Close modal windows
     * and clear all data of user in store.
     */
    closeModal() {
        this.setState({modalIsOpen: false, recoverWindowIsOpen: false});
        this.props.clearUser();
    }

    componentDidMount() {
      let search = new URLSearchParams(this.props.location.search);
      let hash = search.get("hash");
      if (hash) {
        this.openRecoverWindow(hash);
      }
    }

    /**
     * Set user's data to state.
     * @param {Object} e - Target element.
     */
    handleChange(e) {
        const name = e.target.name;
        let value = e.target.value;
        if (name === 'remember') {
          value = !this.state.login.remember;
        }
        this.setState(prevState => ({
            login: {
                ...prevState.login,
                [name]: value,
            },
            errors: {
                ...prevState.errors,
                [name]: null,
            },
            showDummyError: false
        }));
    }

    /**
    * Validate and send form data for login.
    * @param {Object} e - Target element.
    */
    handleSubmit(e) {
    e.preventDefault();
    const {login} = this.state;
    Joi.validate(login, schema, {abortEarly: false})
      .then(() => this.props.login(login))
      .catch(errors => {
        const errorData = makeErrorPath(errors.details || errors);
        errorData.map(error => {
          this.setState(prevState =>({
            errors: {
              ...prevState.errors,
              [error.label]: error.path
            }
          }));
          this.showInactiveError();
          return true;
        })
      })
    }

    /**
     * Show inactive error.
     * @param {String} value - Path to error message.
     */
    showInactiveError() {
      const {email} = this.state.errors;
      if(email){
        const type = email.split('.');
        const lastElement = type.length - 1;
        if (type[lastElement] === 'inactive') {
          this.setState(prevState =>({
            errors: {
              ...prevState.errors,
              email: null
            },
            showDummyError: true
          }));
        }
      }
    }

    /**
     * Request confirm email.
     * @param {String} email - User's email.
     */
    confirmEmailRequest(email){
      this.props.confirmEmailRequest(email)
      .then(response => {
        this.setState({showDummyError: false, showSuccessSend: true});
      })
    }

    renderFields(){
      const { errors, showDummyError, login } = this.state;
      const fields = Object.keys(login);
      return fields.filter(field => field !== 'remember').map(field => {
        return (
          <Input
            type={field === 'email' ? 'text' : 'password'}
            name={field}
            handleChange={this.handleChange}
            icon={field === 'email' ? 'mail' : 'key'}
            error={showDummyError ? '' : errors[field]}
            placeholder={field}
            key={field}
          />
        )
      })
    }

    render() {
        const {t, token} = this.props;
        const { showDummyError, email, showSuccessSend } = this.state;
        let {from} = this.props.location.state || {from: {pathname: '/dashboard'}};
        if ( token.key ) return <Redirect to={from}/>;

        return (
          <div className="App__login">
            <div className="Login">
                <h1 className="App-title">{t('welcome')}</h1>

                {showDummyError && <div className="Login__error">
                  <div>{t('errors.email.type.inactive')}</div>
                    <div>{t('nothingHasCome')} <Link to="/" onClick={() => this.confirmEmailRequest(email)}>{t('resendConfirmation')}</Link></div>
                </div>}

                {showSuccessSend && <div>
                  <div>{t('recoveryPasswordText')}</div>
                </div>}

                <form className="Form">
                    {
                      this.renderFields()
                    }

                    <div className="Form__row Form__row--flex">
                        <label className="checkbox">
                            <input type="checkbox" className="checkbox__native-input" onChange={this.handleChange} name='remember'/>
                            <span className="checkbox__input">
                            </span>
                            <span className="checkbox__label">{t('rememberMe')}</span>
                        </label>
                        <Link to="#" className="Login__forget-link" onClick={this.openRecoverWindow}>{t('recoverPassword')}</Link>
                    </div>

                    <div className="Form__row Form__row--submit">
                        <button type="submit" className="mtl-button mtl-button--rounded mtl-button--orange-solid" onClick={this.handleSubmit}>{t('login')}</button>
                        <span>{t('or')}</span>
                        <button  onClick={this.openModal} type="button" className="mtl-button mtl-button--rounded mtl-button--dark-transparent">{t('registration')}</button>
                    </div>
                </form>
            </div>
            <Modal
                isOpen={this.state.modalIsOpen}
                onRequestClose={this.closeModal}
                style={modalWindow}
            >
                <Registration
                    closeRegistrationWindow={this.closeModal}
                />
            </Modal>

            <Modal
                isOpen={this.state.recoverWindowIsOpen}
                onRequestClose={this.closeModal}
                style={modalWindow}
            >
                <RecoverPassword
                    closeRecoverWindow={this.closeModal}
                    hash={this.state.hash}
                />
            </Modal>
          </div>
        );
    }
}

const schema = {
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  remember: Joi.boolean()
};

function mapStateToProps(state) {
  const token = state.user.get('token').toJS();
  return { token }
}

function mapDispatchToProps(dispatch) {
    return {
        login: bindActionCreators(userActions.login, dispatch),
        confirmEmailRequest: bindActionCreators(userActions.confirmEmailRequest, dispatch),
        clearUser: bindActionCreators(userActions.clearUser, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(Login));
