import React, {Component} from 'react';
import './Checklist.style.scss';
import { toDoListActions } from '../../../actions/toDoList.actions.js';
import { toDoActions } from '../../../actions/toDo.actions.js';
import { assignedUserActions } from '../../../actions/assignedUsers.actions';
import { userActions } from '../../../actions/user.actions';
import { categoriesActions } from '../../../actions/categories.actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from "react-i18next";
import * as Joi from 'joi-browser';
import { makeErrorPath } from '../../../helpers/makeErrorPath';
import Modal from 'react-modal';
import {checklistModalWindow} from "../../../helpers/styles";
import AddAuthor from '../AddAuthor/AddAuthor';
import PropTypes from 'prop-types';
import { randomColor } from '../../../helpers/styles';
import { sharedList } from '../../../helpers/helpers';
import Autocomplete from 'react-autocomplete';

class Checklist extends Component {
  constructor(props){
    super(props);

    this.state = {
      openedMenu: false,
      openedCategory: false,
      title: '',
      newToDo: '',
      showInput: false,
      errors: {
        title: ''
      },
      addUserModal: false,
      border: '',
      currentCategory: '',
      viewCategories: false,
      currentListId: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.addOrEdit = this.addOrEdit.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  componentDidMount(){
    const { category } = this.props;
    if(category){
      this.setState({currentCategory: category})
    }
    this.setState({border: randomColor()});
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  /**
  * Tracks click outside element.
  * @param {Object} event - Target element.
  */
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({openedMenu: false});
    }
  }

  /**
  * Close modal window of authors.
  */
  closeAuthors(){
    this.setState({addUserModal: false});
    this.props.clearCurrentToDoList();
  }

  /**
  * Open modal window of authors.
  * @param {Number} id - List id.
  */
  openAuthors = (id) => {
    this.setState({addUserModal: true, todo_list_id: id})
  }

  /**
  * Delete list.
  * @param {Number} id - List id.
  */
  deleteToDoList(id){
    const { closeModal, deleteToDoList, modalMode } = this.props;
    if(modalMode){
      closeModal()
    }
    deleteToDoList(id);
  }

  /**
  * Set list data to store.
  * @param {Object} e - Target element.
  */
  handleChange(todo_list_id, e, item_id){
    const name = e.target.name;
    const value = e.target.value;
    if(name === 'title' && todo_list_id){
      this.props.updateList({id: todo_list_id, title: value})
    } else {
      this.setState({[name]: value});
    }
    this.setState(prevState =>({
      errors: {
        ...prevState.errors,
        [name]: ''
      }
    }));
  }

  /**
  * Change item status.
  * @param {Object} e - Target element.
  * @param {Number} id - Id of item.
  */
  handleCheck(e, id){
    this.props.editToDoStatus(id);
  }

  /**
  * Validate and add or edit list.
  * @param {Object} e - Target element.
  * @param {Number} id - Id of list.
  */
  addOrEdit(todo_list_id, e, item_id){
    const name = e.target.name;
    const value = e.target.value;
    const { category } = this.props;
    switch (name) {
      case 'title':
        Joi.validate({title: value}, schema, {abortEarly: false})
          .then(() => {
            if(todo_list_id){
              this.props.editListTitle(todo_list_id, value);
            } else {
              this.props.addNewToDoList(value, category)
              .then(response => {
                this.setState({currentListId: response});
              });
            }
          })
          .catch(errors => {
            const errorData = makeErrorPath(errors.details || errors);
            errorData.map(error => {
              this.setState(prevState =>({
                errors: {
                  ...prevState.errors,
                  [error.label]: error.path
                }
              }));
              return true;
            })
          })
        break;
      case 'newToDo':
        if(value){
          this.props.addNewToDo(value, todo_list_id);
          this.setState({newToDo: ''});
        }
        break;
      default:
        if(value && item_id){
          this.setState({['toDoItem_' + item_id]: undefined});
          this.props.editToDoText(item_id, value);
        }
        return false;
    }
  }

  addCategoryToList(title, id){
    this.props.createCategory(title, id);
  }

  /**
  * Delete item.
  * @param {Number} id - Id of list.
  */
  handleDelete(id){
    this.props.deleteToDo(id);
  }

  /**
  * Change menu view.
  */
  toggleMenu() {
    const visibleMenu = this.state.openedMenu;
    this.setState({openedMenu: !visibleMenu});
  }

  handleKeyPress(id, e, item_id) {
    if (e.key === 'Enter') {
      this.addOrEdit(id, e, item_id)
    }
  }

  /**
  * Render items.
  * @param {Array} fields - Array of items.
  * @param {Boolean} checked - Value of check.
  * @return {String} Return html blocks.
  */
  renderItems(fields, checked, todo_list_id){
    const { modalMode } = this.props;
    return fields.map(field => {
      const editedItem = this.state['toDoItem_' + field.id];
      return (
        <li className="Checklist__item" key={field.id}>
          <label>
            <input className="Checklist__item-checkbox" type="checkbox" checked={checked}
            onChange={(e) => this.handleCheck(e, field.id)} name={'toDoItemCheckbox_' + field.id}/>
            <span className="Checklist__item-checkbox-decoration mtl-icon-done"></span>
          </label>
          {
            modalMode
            ? <input type="text" className="Checklist__item-text" name={'toDoItem_' + field.id} value={ editedItem !== undefined ? editedItem : field.text }
              onBlur={(e) => this.addOrEdit(todo_list_id, e, field.id)} onChange={(e) => this.handleChange(todo_list_id, e, field.id)}
              onKeyPress={(e) => this.handleKeyPress(todo_list_id, e, field.id)}/>
            : <span className="Checklist__item-text">{ field.text }</span>
          }
          {
            modalMode &&
            <button className="Checklist__item-delete" onClick={() => this.handleDelete(field.id)}><i className="mtl-icon-cross"></i></button>
          }
        </li>
      )
    })
  }

  renderAuthors(users, invited, user_id){
    return users.map(user => {
      return (
        <span className="Checklist__user" key={ user.email } style={{
          background: '#' + user.color,
          border: user.id === user_id ? this.state.border : '',
          color: '#ffffff',
          fontWeight: 'bold',
          lineHeight: user.id === user_id ? '16px' : '20px'
        }}>
        {
          !invited ? user.firstname.slice(0,1).toUpperCase()+user.lastname.slice(0,1).toUpperCase() : '??'
        }

        </span>
      )
    })
  }

  render() {
    const { openedMenu, errors, viewCategories, title, currentListId } = this.state;
    const { toDoLists, list, modalMode, closeModal, openModal, listId, t, user, makeFavorite, favoriteLists, duplicateList, category, categories } = this.props;
    const newList = list.id || listId ? false : true;
    const currentList = toDoLists.filter( list => {if(list.id === listId || list.id === currentListId) {return list} return false});
    const rendersList = modalMode && currentList[0] ? currentList[0] : list;
    const checkedItems = !!rendersList.todo_items ? rendersList.todo_items.filter( item => item.is_done ) : [];
    const unCheckedItems = !!rendersList.todo_items ? rendersList.todo_items.filter( item => !item.is_done ) : [];
    const favorite = favoriteLists.indexOf(rendersList.id) >= 0;
    return (
      <div className={`Checklist ${modalMode ? 'Checklist--modal' : ''} ${favorite ? 'Checklist--favorite' : ''}`}>

        <div className="Checklist__heading">
          {
            modalMode
            ? <input type="text" className="Checklist__title" value={newList && !rendersList.id ? title : rendersList.title}
                onChange={this.handleChange.bind(this, rendersList.id)} onBlur={this.addOrEdit.bind(this, rendersList.id)} name="title" placeholder={t('newList')}
                autoComplete="off" ref={(input) => { this.titleInput = input; }}  onKeyPress={this.handleKeyPress.bind(this, rendersList.id)}/>
            : <span className="Checklist__title">{rendersList.title}</span>
          }

          <div className="Checklist__heading-addon">
            { modalMode &&
              <button className={`Checklist__fav-btn ${favorite ? 'Checklist__fav-btn--selected' : ''}`} onClick={ () => { makeFavorite(rendersList.id) } }>
                {
                  favorite
                  ? <i className="mtl-icon-star-fill"></i>
                  : <i className="mtl-icon-star"></i>
                }
              </button>
            }

            <div className={`Checklist__options ${openedMenu ? 'Checklist__options--expanded' : ''}`} ref={ el => this.wrapperRef = el } >
              <button className="Checklist__options-btn" onClick={() => this.toggleMenu()}><i className="mtl-icon-dots"></i></button>
              <ul className="Checklist__options-menu">
                {
                  !modalMode && rendersList.user_id === user.id &&
                  <li className="Checklist__option">
                    <span onClick={ () => { this.toggleMenu(); openModal() } }>{t('editList')}</span>
                  </li>
                }
                <li className="Checklist__option">
                  <span onClick={ () => { this.toggleMenu(); makeFavorite(rendersList.id) } }>{!favorite ? t('addToFavorites') : t('deleteToFavorites')}</span>
                </li>
                {
                  !user.is_free
                  ? <li className="Checklist__option">
                      <span onClick={ () => { this.toggleMenu(); this.openAuthors(rendersList.id) } }>{t('shareList')}</span>
                    </li>
                  : ""
                }

                <li className="Checklist__option">
                  <span onClick={ () => { this.toggleMenu(); duplicateList(rendersList.id) } }>{t('duplicateList')}</span>
                </li>

                <li className="Checklist__option Checklist__option--del">
                  <span onClick={ () => {
                    this.toggleMenu();
                    rendersList.user_id === user.id ? this.deleteToDoList(rendersList.id) : this.props.deleteShare(user.id, rendersList.id, false)} }
                  >{t('deleteList')}</span>
                </li>

              </ul>
            </div>
          </div>
        </div>
        {
          modalMode && errors.title &&
          <div className="input__feedback error">{t(errors.title)}</div>
        }
        <ul className="Checklist__items">
          {
            this.renderItems(unCheckedItems, false, rendersList.id)
          }
        </ul>
        { checkedItems.length > 0 &&
          <ul className="Checklist__items Checklist__items--done">
            {
              this.renderItems(checkedItems, true, rendersList.id)
            }
          </ul>
        }
        {
          modalMode && rendersList.id &&
          <input type="text" className="Checklist__item-text" placeholder={t('newItem')} name="newToDo"
          onBlur={this.addOrEdit.bind(this, rendersList.id)} onChange={this.handleChange.bind(this, rendersList.id)} value={this.state.newToDo}
          onKeyPress={this.handleKeyPress.bind(this, rendersList.id)}
          />
        }
        { sharedList(rendersList.assigned_users, 1) || sharedList(rendersList.invited_users, 0)
          ? <div className="Checklist__users">
              {
                sharedList(rendersList.assigned_users, 1) &&
                this.renderAuthors(rendersList.assigned_users, false, rendersList.user_id)
              }
              {
                sharedList(rendersList.invited_users, 0) &&
                this.renderAuthors(rendersList.invited_users, true, rendersList.user_id)
              }
              <button className="Checklist__add-user-btn" onClick={ () => { this.openAuthors(rendersList.id) } }>+</button>
            </div>
          : ''
        }
        {
          modalMode && listId && rendersList.updated_at &&
            <div className="Checklist__edit-date">{t('changed')} {rendersList.updated_at.slice(0,10)}</div>
        }
        { modalMode &&
          <div className="Checklist__footer">
            {
              viewCategories || category
              ? <Autocomplete
                  items={categories}
                  shouldItemRender={(category, value) => category.title.toLowerCase().indexOf(value.toLowerCase()) > -1}
                  getItemValue={category => category.title}
                  renderItem={(category, highlighted) =>
                    <div
                      key={category.title}
                      style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}
                      className="Checklist-categories__list-item"
                    >
                      {category.title}
                    </div>
                  }
                  inputProps={{
                    className: "Checklist-categories__input",
                    name: 'email',
                    onBlur: (e) => this.addCategoryToList(e.target.value, rendersList.id)
                  }}
                  menuStyle={{
                    listStyle: 'none',
                    margin: '0',
                    padding: '15px',
                    position: 'absolute',
                    top: '100%',
                    left: '-15px',
                    right: '-15px',
                    borderRadius: '5px',
                    backgroundColor: '#ffffff',
                    display: !!categories.length ? 'block' : 'none'
                  }}
                  value={this.state.currentCategory}
                  onChange={(e) => this.setState({currentCategory: e.target.value})}
                  onSelect={value => this.setState({currentCategory: value})}
                  wrapperProps={{
                    className: "Checklist-categories Checklist-categories--has-autocomplete"
                  }}
                />
              : <span className="Checklist-categories__list-item" style={{cursor: 'pointer'}} onClick={() => this.setState({viewCategories: true})}>{t('addCategory')}</span>
            }

            <button className="Checklist__close-btn" onClick={()=> closeModal() }>{t('close')}</button>
          </div>
        }

        <Modal isOpen={ this.state.addUserModal } style={checklistModalWindow}  onRequestClose={() => this.closeAuthors()}>
          <AddAuthor
            closeAuthors={ () => this.closeAuthors() }
            todo_list_id={this.state.todo_list_id}
            assignedUsers={rendersList.assigned_users}
            invitedUsers={rendersList.invited_users}
            user_id={rendersList.user_id}
          />
        </Modal>

      </div>
    )
  }
}

Checklist.propTypes = {
  list: PropTypes.object,
  listId: PropTypes.number,
  modalMode: PropTypes.bool,
  closeModal: PropTypes.func,
  openModal: PropTypes.func,
  category: PropTypes.string
}

const schema = {
  title: Joi.string().required()
};

function mapStateToProps(state) {
  const user = state.user.get('data').toJS();
  const favoriteLists = state.user.get('favoriteLists').toJS();
  const categories = state.categories.get('categories').toJS();
  const toDoLists = state.toDoList.get('toDoLists').toJS();
  return { user, favoriteLists, toDoLists, categories }
}

function mapDispatchToProps(dispatch) {
  return {
    addNewToDoList: bindActionCreators(toDoListActions.addNewToDoList, dispatch),
    editListTitle: bindActionCreators(toDoListActions.editListTitle, dispatch),
    addNewToDo: bindActionCreators(toDoActions.addNewToDo, dispatch),
    setCurrentToDoList: bindActionCreators(toDoListActions.setCurrentToDoList, dispatch),
    getCurrentToDoList: bindActionCreators(toDoListActions.getCurrentToDoList, dispatch),
    editToDoStatus: bindActionCreators(toDoActions.editToDoStatus, dispatch),
    deleteToDo: bindActionCreators(toDoActions.deleteToDo, dispatch),
    editToDoText: bindActionCreators(toDoActions.editToDoText, dispatch),
    deleteToDoList: bindActionCreators(toDoListActions.deleteToDoList, dispatch),
    deleteShare: bindActionCreators(assignedUserActions.deleteShare, dispatch),
    makeFavorite: bindActionCreators(userActions.makeFavorite, dispatch),
    duplicateList: bindActionCreators(toDoListActions.duplicateList, dispatch),
    createCategory: bindActionCreators(categoriesActions.createCategory, dispatch),
    clearCurrentToDoList: bindActionCreators(toDoListActions.clearCurrentToDoList, dispatch),
    updateList: bindActionCreators(toDoListActions.updateList, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(Checklist));
