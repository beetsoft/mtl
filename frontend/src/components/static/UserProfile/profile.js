import React, {Component} from 'react';
import Modal from 'react-modal';
import {roundedModalWindow} from "../../../helpers/styles";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {userActions} from '../../../actions/user.actions';
import {withNamespaces} from 'react-i18next';
import Input from '../../helperComponents/input';
import * as Joi from 'joi-browser';
import {makeErrorPath} from '../../../helpers/makeErrorPath';
import Autocomplete from 'react-google-autocomplete';
import ChangePassword from './changePassword';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        firstname: '',
        lastname: '',
        email: ''
      },
      address: '',
      id: '',
      errors: {},

      passwordModalOpened: false
    }

    this.handleChangeUser = this.handleChangeUser.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeAddress = this.handleChangeAddress.bind(this);
    this.placeSelected = this.placeSelected.bind(this);
    this.closePasswordModal = this.closePasswordModal.bind(this);
  };

  closePasswordModal() {
    this.setState({passwordModalOpened: false})
  }

  openPasswordModal() {
    this.setState({passwordModalOpened: true})
  }

  static getDerivedStateFromProps(props, state) {
    if (props.userData.id !== state.id) {
      return {
        user: {
          ...state.user,
          email: props.userData.email,
          firstname: props.userData.firstname,
          lastname: props.userData.lastname,
        },
        address: props.userData.address,
        id: props.userData.id
      };
    }
   return null;
 }

  /**
  * Set user's data to state.
  * @param {Object} e - Target element.
  */
  handleChangeUser(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        [name]: null,
        address: null
      },
      user: {
        ...prevState.user,
        [name]: value
      }
    }));
  }

  handleChangeAddress(e) {
    this.setState({address: e.target.value});
    this.setState(prevState => ({
      errors: {
        address: null
      },
    }));
  }

  /**
  * Validate and send form data to store.
  * @param {Object} e - Target element.
  */
  handleSubmit(e) {
    e.preventDefault();
    const {user, address} = this.state;
    const data = Object.assign({}, user, {address: address});
    Joi.validate(data, schema, {abortEarly: false}).then(() => this.props.editUserInfo(data)).catch(errors => {
      const errorData = makeErrorPath(errors.details || errors);
      errorData.map(error => {
        this.setState(prevState => ({
          errors: {
            ...prevState.errors,
            [error.label]: error.path
          }
        }));
        return true;
      })
    })
  }

  placeSelected(place) {
    const address = place.formatted_address;
    this.setState(prevState => ({
      errors: {
        address: null
      },
      address: address
    }));
  }

  /**
  * Render input fields.
  * @return {String} Return html blocks.
  */
  renderFields() {
    const {user, errors} = this.state;
    const fields = Object.keys(user);
    return fields.map(field => {
      return (<Input type='text' name={field} handleChange={this.handleChangeUser.bind(this)} error={errors[field]} key={field} value={user[field]} placeholder={field}/>);
    });
  }

  render() {
    const {passwordModalOpened, errors} = this.state;
    const {t} = this.props;
    return (<div className="UserProfile__tab UserProfile__tab--profile">
      <form className="Form" onSubmit={this.handleSubmit}>
        {this.renderFields()}
        <div className="Form__row">
          <div className="input input--empty-value">
            <div className={`input__field-wrap ${errors.address
                ? 'error'
                : ''}`}>
              <Autocomplete className='input__field' placeholder={t('enterAddress')} onPlaceSelected={(place) => {
                  this.placeSelected(place)
                }} types={['address']} value={this.state.address || ''} onChange={this.handleChangeAddress}/>
            </div>
          </div>
        </div>
        {errors.address && <div className="input__feedback error">{t(errors.address)}</div>}
        <div className="UserProfile__submit">
          <button className="mtl-button mtl-button--rounded mtl-button--orange-solid">{t('saveChanges')}</button>
          <span onClick={() => this.openPasswordModal()} style={{
              cursor: 'pointer'
            }}>{t('changePassword')}</span>
        </div>
      </form>
      <Modal isOpen={passwordModalOpened} onRequestClose={() => this.closePasswordModal()} style={roundedModalWindow}>
        <ChangePassword closePasswordModal={this.closePasswordModal}/>
      </Modal>
    </div>);
  }
}

const schema = {
  email: Joi.string().email().required(),
  firstname: Joi.string().min(3).max(30).required(),
  lastname: Joi.string().min(3).max(30).required(),
  address: Joi.string().min(10).max(100).required()
};

function mapStateToProps(state) {
  const userData = state.user.get('data').toJS();
  const address = state.user.get('address');
  return {userData, address}
}

function mapDispatchToProps(dispatch) {
  return {
    editUserInfo: bindActionCreators(userActions.editUserInfo, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(Profile));
