import React, {Component} from 'react';
import './UserProfile.style.scss';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {userActions} from '../../../actions/user.actions';
import {withNamespaces} from 'react-i18next';
import CheckoutForm from '../../registration/checkoutForm';
import {Elements} from 'react-stripe-elements';

class PaymentTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ''
    }

    this.successPayment = this.successPayment.bind(this);
  };

  /*
  * Set payment in state if payment success.
  */
  successPayment = (token) => {
    const { t } = this.props;
    this.props.upgradeUserAccount(token).catch(error => {
      this.setState({ message: t('paymentFailed')});
    });

  }

  render() {
    const { user, t } = this.props;
    const { message } = this.state;
    if(user.is_free){
      return (<div className="UserProfile__tab UserProfile__tab--renew">
        <div className="UserProfile__renew">
        <div className="UserProfile__renew-title">{t('premium')}</div>
        {t('premiumText')}
        </div>
        <Elements>
          <CheckoutForm successPayment={this.successPayment} />
        </Elements>
        {
          message && <div className="input__feedback error">{message}</div>
        }
      </div>);
    } else {
      return (<div className="UserProfile__tab UserProfile__tab--renew">
        <div className={"UserProfile__renew UserProfile__renew--done"}>
          {t('premiumTrue')}
        </div>
      </div>);
    }
  }
}

function mapStateToProps(state) {
  const user = state.user.get('data').toJS();
  return {user}
}

function mapDispatchToProps(dispatch) {
  return {
    upgradeUserAccount: bindActionCreators(userActions.upgradeUserAccount, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(PaymentTab));
