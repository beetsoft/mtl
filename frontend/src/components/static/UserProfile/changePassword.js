import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { userActions } from '../../../actions/user.actions';
import { withNamespaces } from 'react-i18next';
import Input from '../../helperComponents/input';
import * as Joi from 'joi-browser';
import {makeErrorPath} from '../../../helpers/makeErrorPath';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passwords: {
        password: '',
        newPassword: '',
        confirmPassword: ''
      },
      errors: {}
    }

    this.changePassword = this.changePassword.bind(this);
  };

  handleChangePassword(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        [name]: null
      },
      passwords: {
        ...prevState.passwords,
        [name]: value
      }
    }));
  }

  /**
  * Validate and send form data to store.
  * @param {Object} e - Target element.
  */
  changePassword(e) {
    e.preventDefault();
    const {passwords} = this.state;
    const {closePasswordModal} = this.props;
    Joi.validate(passwords, schema, {abortEarly: false})
      .then(() => this.props.addNewPassword(passwords))
      .then((response) => {
        if(response.isSuccess){
          closePasswordModal();
        }
      })
      .catch(errors => {
      const errorData = makeErrorPath(errors.details || errors);
      errorData.map(error => {
        this.setState(prevState => ({
          errors: {
            ...prevState.errors,
            [error.label]: error.path
          }
        }));
        return true;
      })
    })
  }

  /**
  * Render input fields.
  * @return {String} Return html blocks.
  */
  renderFields() {
    const {passwords, errors} = this.state;
    const fields = Object.keys(passwords);
    return fields.map(field => {
      return (<Input type='password' name={field} handleChange={this.handleChangePassword.bind(this)} error={errors[field]} key={field} value={passwords[field]} placeholder={field}/>);
    });
  }

  render() {
    const { closePasswordModal } = this.props;
    const {t} = this.props;
    return (
      <div className="UserProfile__modal">
        <button className="modal-close-btn mtl-icon-cross" onClick={() => closePasswordModal()}></button>
        <h3 className="UserProfile__modal-title">{t('changePasswordModal')}</h3>
        <form className="Form" onSubmit={this.changePassword}>
          {
            this.renderFields()
          }
          <button type="submit" className="mtl-button mtl-button--rounded mtl-button--orange-solid">{t('saveChanges')}</button>
        </form>
      </div>
    );
  }
}

const schema = {
  password: Joi.string().min(6).max(30).required(),
  newPassword: Joi.string().min(6).max(30).required(),
  confirmPassword: Joi.string().valid(Joi.ref('newPassword')).required()
};

function mapStateToProps(state) {
  const userData = state.user.get('data').toJS();
  const address = state.user.get('address');
  return { userData, address }
}

function mapDispatchToProps(dispatch) {
  return {
    addNewPassword: bindActionCreators(userActions.addNewPassword, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(ChangePassword));
