import React, {Component} from 'react';
import './UserProfile.style.scss';
import Profile from './profile';
import PaymentTab from './paymentTab';
import {withNamespaces} from 'react-i18next';

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTabId: 'profile',
      tabs: []
    }
  };

  componentDidMount(){
    const tabs = [ 'profile', 'renew' ];
    tabs.map( tab => {
      this.setState(prevState => ({
        tabs: [
          ...prevState.tabs,
          {
            name: tab
          }
        ]
      }))
      return false;
    })
  }

  changeTab(tab) {
    this.setState({activeTabId: tab})
  }

  render() {
    const {tabs, activeTabId} = this.state;
    const {t} = this.props;
    return (<div className="UserProfile">
      <h1 className="App-title App-title--small">{t('personalArea')}</h1>
      <div className="UserProfile__tabs">
        <div className="UserProfile__tabs-header">
          {
            tabs.map((tab) => {
              return (<button key={tab.name} onClick={() => this.changeTab(tab.name)} className={`UserProfile__tabs-btn ${activeTabId === tab.name
                  ? 'UserProfile__tabs-btn--active'
                  : ''}`}>
                {t(tab.name)}
              </button>)
            })
          }
        </div>
        <div className="UserProfile__tabs-content">
          {
            activeTabId === 'profile' && <Profile/>
          }
          {
            activeTabId === 'renew' && <PaymentTab />
          }
        </div>
      </div>
    </div>);
  }
}

export default withNamespaces()(UserProfile);
