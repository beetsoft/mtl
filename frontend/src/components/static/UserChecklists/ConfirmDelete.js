import React, { Component } from 'react';
import { categoriesActions } from '../../../actions/categories.actions.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from "react-i18next";
import PropTypes from 'prop-types';

class ConfirmDelete extends Component {

  deleteSubmit(e){
    e.preventDefault();
    const {currentCatId, deleteCategory, closeModal} = this.props;
    deleteCategory(currentCatId);
    closeModal();
  }

  render(){
    const {t, closeModal} = this.props;
    return (
      <div className="UserProfile__modal">
        <button className="modal-close-btn mtl-icon-cross" onClick={() => closeModal()}></button>
        <h3 className="UserProfile__modal-title">{t('deletingCategory')}</h3>
        <div className="UserChecklists__delete">{t('youSure')}</div>
        <div className="UserChecklists__delete">{t('deleteText')}</div>
        <form className="Form" onSubmit={this.deleteSubmit.bind(this)}>
          <button type="submit" className="mtl-button mtl-button--rounded mtl-button--orange-solid">{t('deleteCategory')}</button>
        </form>
      </div>
    )
  }
}

ConfirmDelete.propTypes = {
  currentCatId: PropTypes.number,
  deleteCategory: PropTypes.func,
  closeModal: PropTypes.func
}

function mapDispatchToProps(dispatch) {
  return {
    deleteCategory: bindActionCreators(categoriesActions.deleteCategory, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(withNamespaces()(ConfirmDelete))
