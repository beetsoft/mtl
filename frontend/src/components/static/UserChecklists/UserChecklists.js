import React, { Component } from 'react';
import './UserChecklists.style.scss';
import Checklist from '../Checklist/Checklist';
import Modal from 'react-modal';
import {checklistModalWindow} from "../../../helpers/styles";
import ChecklistFilter from '../ChecklistFilter/ChecklistFilter';
import { toDoListActions } from '../../../actions/toDoList.actions.js';
import { categoriesActions } from '../../../actions/categories.actions.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from "react-i18next";
import { sharedList } from '../../../helpers/helpers';
import ConfirmDelete from './ConfirmDelete';

class UserChecklists extends Component {

  constructor(props){
    super(props);
    this.state = {
      checklistModal: false,
      listId: null,
      page: 1,
      prevY: 0,
      filter: '',
      editMode: false,
      title: '',
      currentCategory: '',
      currentCatId: '',
      deleteCategory: false,
      loadError: false
    }
  }

  componentDidMount(){
    this.props.getToDoLists()
    .catch(errors => {
      this.setState({loadError: errors[0].path});
    });
    var options = {
      root: null,
      rootMargin: "0px",
      threshold: 1.0
    };
    this.observer = new IntersectionObserver(
      this.handleObserver.bind(this),
      options
    );
    this.observer.observe(this.loadingRef);
  }

  componentWillUnmount() {
    this.observer.unobserve(this.loadingRef);
  }

  /**
  * Tracks end of page.
  * @param {Array} entities - Data of observer.
  * @param {Object} observer - Observer.
  */
  handleObserver(entities, observer) {
    const y = entities[0].boundingClientRect.y;
    if (this.state.prevY > y) {
      const curPage = 1+(+this.state.page);
      this.setState({ page: curPage });
    }
    this.setState({ prevY: y });
  }

  handleChange(id, e){
    const value = e.target.value;
    this.props.changeCategoryTitle({id: id, title: value});
  }

  editCategory(title, id){
    const {editCategory} = this.props;
    if(title){
      editCategory(id, title);
      this.setState({editMode: false})
    }
  }

  /**
  * Open modal window of checklist.
  * @param {Number} listId - List id.
  */
  openModal(listId, title, catId) {
    this.setState({checklistModal: true, listId: listId, currentCategory: title, currentCatId: catId})
  }

  openDeleteCategoryModal(id){
    this.setState({deleteCategory: true, currentCatId: id})
  }

  /**
  * Close modal window of checklist.
  */
  closeModal() {
    this.props.clearCurrentToDoList();
    this.props.getToDoLists();
    this.setState({checklistModal: false, deleteCategory: false})
  }

  sortFavorite(prevList, nextlist){
    const { favoriteLists } = this.props;
    return favoriteLists.indexOf(nextlist.id) - favoriteLists.indexOf(prevList.id);
  }

  ownFilter(){
    const { toDoLists, user } = this.props;
    const { filter } = this.state;
    switch (filter) {
      case 'own':
        return toDoLists.filter(list => list.user_id === user.id);
      case 'shared':
        return toDoLists.filter(list => list.user_id !== user.id && (sharedList(list.assigned_users, 1) || sharedList(list.invited_users, 0)));
      default:
        return toDoLists;
    }
  }

  filterList = (choice) => {
    this.setState({filter: choice})
  }

  renderCategories(){
    const {categories} = this.props;
    const {page, editMode, currentCatId} = this.state;
    return categories.map(cat => {
      return (
        <div key={cat.id}>
          <div className="UserChecklists__content-title">
            {
              editMode && currentCatId === cat.id
              ? <input type="text"
                  className="Checklist__item-text"
                  onChange={this.handleChange.bind(this, cat.id)}
                  value={cat.title}
                  onBlur={() => this.editCategory(cat.title, cat.id)}
                  onFocus={() => this.setState({currentCatId: cat.id})}
                />
              : <span>{cat.title}</span>
            }

            <button className="UserChecklists__add-btn" onClick={() => this.openModal(null, cat.title)}>+</button>
            <div className="UserChecklists__actions">
              <button className="UserChecklists__actions-btn" onClick={() => this.setState({editMode: true, currentCatId: cat.id})}><span className="mtl-icon-pen"></span></button>
              <button className="UserChecklists__actions-btn UserChecklists__actions-btn--del" onClick={() => this.openDeleteCategoryModal(cat.id)}><span className="mtl-icon-trash"></span></button>
            </div>
          </div>
          <div className="UserChecklists__content">
            {
              this.ownFilter().sort(this.sortFavorite.bind(this)).filter( ( list, index ) => index < page*32  ).map( (list, index) => {
                if(cat.id === list.category_id){
                  return (
                    <div className="UserChecklists__column" key={list.id}>
                      <Checklist list={list} openModal={ () => this.openModal(list.id, cat.title, cat.id) }/>
                    </div>
                  )
                }
                return false;
              })
            }
          </div>
        </div>
      )
    })
  }

  render () {
    const { t, user } = this.props;
    const { listId, page, loadError } = this.state;
    return (
      <div className="UserChecklists">
        <div className="UserChecklists__header">
          <input type="text" className="UserChecklists__input" placeholder={t('newList')} onClick={() => this.openModal()}/>
          <div className="UserChecklists__filter">
            <ChecklistFilter filterList={this.filterList}/>
          </div>
        </div>
        <div className="UserChecklists__content-title">
          <span className="Checklist__item-text">{t('noCategory')}</span>
        </div>
        <div className="UserChecklists__content">
          {
            this.ownFilter().sort(this.sortFavorite.bind(this)).filter( ( list, index ) => index < page*32 && (!list.category_id || user.id !== list.user_id) ).map( (list, index) => {
              return (
                <div className="UserChecklists__column" key={list.id}>
                  <Checklist list={list} openModal={ () => this.openModal(list.id) }/>
                </div>
              )
            })
          }
        </div>
        {
          this.renderCategories()
        }
        <Modal isOpen={ this.state.checklistModal } style={checklistModalWindow} onRequestClose={() => this.closeModal()}>
          <div className="UserChecklists__checklist-modal">
            <Checklist list={{}} listId={listId} modalMode={true} closeModal={() => this.closeModal()}  category={this.state.currentCategory} currentCatId={this.state.currentCatId}/>
          </div>
        </Modal>
        <Modal isOpen={ this.state.deleteCategory } style={checklistModalWindow} onRequestClose={() => this.closeModal()}>
          <div className="UserChecklists__checklist-modal">
            <ConfirmDelete currentCatId={this.state.currentCatId} closeModal={() => this.closeModal()} />
          </div>
        </Modal>
        <div
          ref={loadingRef => (this.loadingRef = loadingRef)}
        >
        {
          loadError && !user.id &&
          <div>{t(loadError)}</div>
        }
        </div>
      </div>
    )

  }
}

function mapStateToProps(state) {
  const user = state.user.get('data').toJS();
  const favoriteLists = state.user.get('favoriteLists').toJS();
  const toDoLists = state.toDoList.get('toDoLists').toJS();
  const categories = state.categories.get('categories').toJS();
  return { toDoLists, user, favoriteLists, categories }
}

function mapDispatchToProps(dispatch) {
  return {
    getToDoLists: bindActionCreators(toDoListActions.getToDoLists, dispatch),
    getCurrentToDoList: bindActionCreators(toDoListActions.getCurrentToDoList, dispatch),
    clearCurrentToDoList: bindActionCreators(toDoListActions.clearCurrentToDoList, dispatch),
    editCategory: bindActionCreators(categoriesActions.editCategory, dispatch),
    changeCategoryTitle: bindActionCreators(categoriesActions.changeCategoryTitle, dispatch),
    deleteCategory: bindActionCreators(categoriesActions.deleteCategory, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(UserChecklists));
