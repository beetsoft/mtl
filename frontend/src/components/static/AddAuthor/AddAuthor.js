import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { assignedUserActions } from '../../../actions/assignedUsers.actions';
import { withNamespaces } from 'react-i18next'
import './AddAuthor.style.scss';
import Autocomplete from 'react-autocomplete';
import { makeErrorPath } from '../../../helpers/makeErrorPath';
import PropTypes from 'prop-types';
import { randomColor } from '../../../helpers/styles';
import * as Joi from 'joi-browser';

class AddAuthor extends Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      errors: {},
      border: ''
    }

    this.handleChange = this.handleChange.bind(this);
  }

  /**
  * Set users email data to state.
  * @param {Object} e - Target element.
  */
  handleChange(e){
    const value = e.target.value;
    this.setState({errors: {}, email: value});
    if(value){
      this.props.getUsersByEmail(value);
    } else {
      this.props.clearUsers();
    }
  }

  /**
  * Share list.
  */
  shareList(){
    const { email } = this.state;
    const { todo_list_id } = this.props;
    if(email && todo_list_id){
      Joi.validate({email: email}, schema, {abortEarly: false})
        .then(() => this.props.shareList(todo_list_id, email))
        .catch(errors => {
          const errorData = makeErrorPath(errors.details || errors);
          errorData.map(error => {
            this.setState(prevState =>({
              errors: {
                ...prevState.errors,
                [error.label]: error.path
              }
            }));
            return true;
          })
        })
    }
  }

  componentDidMount(){
    this.setState({border: randomColor()});
  }

  /**
  * Render authors list.
  * @return {String} Return html blocks.
  */
  renderAuthors(users, invited){
    const { user_id, currentUser, todo_list_id } = this.props;
      return users.map( user => {
        return (
          <div className="AddAuthor__item" key={user.id}>
            <div className="AddAuthor__item-img">
              <span className="Checklist__user" key={ user.email } style={{
                background: '#' + user.color,
                border: user.id === user_id ? this.state.border : '',
                color: '#ffffff',
                fontWeight: 'bold',
                lineHeight: user.id === user_id ? '16px' : '20px'
              }}>
              {
                !invited ? user.firstname.slice(0,1).toUpperCase()+user.lastname.slice(0,1).toUpperCase() : '??'
              }

              </span>
            </div>
            <div className="AddAuthor__item-text">
              {!invited && user.firstname + " " + user.lastname}
              <div className="AddAuthor__item-mail">{user.email}</div>
            </div>
            {
              user_id !== user.id && currentUser.id === user_id
              ? <button className="AddAuthor__item-btn" onClick={ () => this.props.deleteShare(user.id, todo_list_id, invited) }><i className="mtl-icon-cross"></i></button>
              : ''
            }

          </div>
        )
      })
  }

  renderErrors(){
    const { t } = this.props;
    const { errors } = this.state;
    const keys = Object.keys(errors);
    if(keys){
      return <div className="input__feedback error">{t(errors[keys[0]])}</div>
    }
  }

  render() {
    const { closeAuthors, t, users, invitedUsers, assignedUsers } = this.props;
    const { email } = this.state;
    return (
      <div className="AddAuthor">
        <h4 className="AddAuthor__title">{t('collaborators')}</h4>

        <div className="AddAuthor__body">

          {
            assignedUsers && assignedUsers.length > 1 &&
            this.renderAuthors(assignedUsers, false)
          }
          {
            invitedUsers && invitedUsers.length > 0 &&
            this.renderAuthors(invitedUsers, true)
          }

          <div className="AddAuthor__item AddAuthor__item--new">
            <div className="AddAuthor__item-img"></div>
            <div className="AddAuthor__item-text" style={{position: 'relative'}}>
              <Autocomplete
                items={users}
                shouldItemRender={(user, value) => user.email.toLowerCase().indexOf(value.toLowerCase()) > -1}
                getItemValue={user => user.email}
                renderItem={(user, highlighted) =>
                  <div
                    key={user.email}
                    style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}
                  >
                    {user.email}
                  </div>
                }
                inputProps={{
                  className: "AddAuthor__item-input",
                  placeholder: t('email'),
                  name: 'email'
                }}
                menuStyle={{
                  borderRadius: '3px',
                  boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                  background: 'rgba(255, 255, 255, 0.9)',
                  padding: '2px 0',
                  fontSize: '100%',
                  position: 'absolute',
                  overflow: 'auto',
                  top: '11',
                  left: '0',
                  display: !!users.length ? 'block' : 'none'
                }}
                value={email}
                onChange={this.handleChange}
                onSelect={value => this.setState({ email: value })}
              />
              {
                this.renderErrors()
                // errors.own_list &&
                // <div className="input__feedback error">{t('own_list')}</div>
              }
            </div>
            {
              email &&
              <button className="AddAuthor__item-btn" onClick={() => this.shareList()}><i className="mtl-icon-cross"></i></button>
            }
          </div>
        </div>

        <div className="AddAuthor__footer">
          <button className="AddAuthor__btn" onClick={() => closeAuthors()}>{t('close')}</button>
        </div>
      </div>
    )
  }
}

AddAuthor.propTypes = {
  assignedUsers: PropTypes.array,
  todo_list_id: PropTypes.number,
  closeAuthors: PropTypes.func,
  user_id: PropTypes.number
}

const schema = {
  email: Joi.string().email().required()
};

function mapStateToProps(state) {
  const currentUser = state.user.get('data').toJS();
  const users = state.assignedUsers.get('users').toJS();
  return { users, currentUser }
}

function mapDispatchToProps(dispatch) {
  return {
    getUsersByEmail: bindActionCreators(assignedUserActions.getUsersByEmail, dispatch),
    clearUsers: bindActionCreators(assignedUserActions.clearUsers, dispatch),
    shareList: bindActionCreators(assignedUserActions.shareList, dispatch),
    deleteShare: bindActionCreators(assignedUserActions.deleteShare, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(AddAuthor));
