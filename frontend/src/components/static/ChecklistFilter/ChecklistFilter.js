import React, {Component} from 'react';
import './ChecklistFilter.style.scss';
import { withNamespaces } from "react-i18next";

class ChecklistFilter extends Component {
  constructor(props){
    super(props);
    this.state = {
      openedFilter: false,
      filter: 'all',
      filters: ['all', 'own', 'shared']
    }

    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  toggleFilter() {
    const visibleFilter = this.state.openedFilter;
    this.setState({openedFilter: !visibleFilter});
  }

  componentDidMount(){
    this.filterList('all');

    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({openedFilter: false});
    }
  }

  filterList(choise){
    this.setState({filter: choise});
    this.toggleFilter();
    this.props.filterList(choise);
  }

  renderFilter(){
    const { filters, filter } = this.state;
    const { t } = this.props;
    return filters.map( choise => {
      return (
        <li className={`ChecklistFilter__item ${filter === choise ? 'ChecklistFilter__item--selected' : ''}`}
        key={choise} onClick={ () => { this.filterList(choise) } }>{t('filter.'+choise)}</li>
      )
    })
  }

  render() {
    const { openedFilter } = this.state;
    const { t } = this.props;
    return (
      <div className="ChecklistFilter">
        <div className="ChecklistFilter__inner" ref={ el => this.wrapperRef = el }>
          <button className="ChecklistFilter__current" onClick={() => {this.toggleFilter()}}>{t('filtrate')}</button>
          <ul className="ChecklistFilter__items" style={openedFilter ? {} : {display: 'none'}}>
            {
              this.renderFilter()
            }
          </ul>
        </div>
      </div>
    )
  }
}

export default withNamespaces()(ChecklistFilter);
