import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import Modal from 'react-modal';
import { modalWindow } from '../helpers/styles';
import Registration from './registration/registration';
import { userActions } from '../actions/user.actions'
import { withNamespaces } from "react-i18next";
import '../assets/styles/header.style.scss';
import { Link } from 'react-router-dom';
import { withRouter } from "react-router";

Modal.setAppElement('body');

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: false,
      language: '',
      langSelectorOpened: false
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  /**
  * Open modal window for registration.
  */
  openModal() {
    this.setState({modalIsOpen: true});
  }

  /**
  * Close modal window for registration
  * and clear all data of user in store.
  */
  closeModal() {
    this.setState({modalIsOpen: false});
    this.props.clearUser();
  }

  componentDidMount(){
    const { language } = this.props.i18n;
    const { data } = this.props;
    this.setState({ language: language });
    if(data.id){
      this.props.getCurrentUser();
    }
  }

  /**
  * Set data to state and change language on page.
  * @param {Object} e - Target element.
  */
  handleChange(lang){
    this.setState({language: lang, langSelectorOpened: false});
    this.props.i18n.changeLanguage(lang);
  }

  /**
  * Returns other languages which you can select.
  * @return {String} Return html blocks.
  */
  renderOtherLangs(){
    const { language } = this.state;
    return this.props.langs.filter(lang => lang !== language).map(lang => {
      return (
        <div className="LangSelector__options" onClick={() => this.handleChange(lang)} key={lang}>
          <span className={`LangSelector__flag LangSelector__flag--${lang}`}></span>
        </div>
      )
    })
  }

  render(){
    const { t, data, history } = this.props;
    const { langSelectorOpened, language } = this.state;
    return (
      <div className="Header">
        <div className="Header__inner">
          <Link to="/" className="Header__logo"></Link>
          {
            data.id
            ? <div className="Header__user">
                <Link to="/user-profile" className="Header__username">{data.firstname}</Link>
                <Link className="Header__user-link" to="#" onClick={() => {this.props.logout()}}>{t('logOut')}</Link>
              </div>
            : <div className="Header__user">
                <button className="mtl-button mtl-button--orange-solid mtl-button--rounded" onClick={() => {history.push("/")}}>{t('login')}</button>
                <button className="mtl-button mtl-button--white-transparent mtl-button--rounded" onClick={this.openModal}>{t('registration')}</button>
              </div>
          }
          <div className={`LangSelector ${langSelectorOpened ? 'LangSelector--expanded' : ''}`}>
            <div className="LangSelector__inner">
              <button className="LangSelector__current" onClick={()=>this.setState({langSelectorOpened: !langSelectorOpened})}>
                <span className={`LangSelector__flag LangSelector__flag--${language}`}></span>
                <span className="LangSelector__toggler mtl-icon-angle-down"></span>
              </button>
              {this.renderOtherLangs()}
            </div>
          </div>
        </div>

        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          style={modalWindow}
        >
          <Registration
            closeRegistrationWindow={this.closeModal}
          />
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { user } = state;
  const langs = user.get('langs').toJS();
  const token = user.get('token').toJS();
  const data = user.get('data').toJS();
  return { langs, token, data }
}

function mapDispatchToProps(dispatch){
  return {
    clearUser: bindActionCreators(userActions.clearUser, dispatch),
    logout: bindActionCreators(userActions.logout, dispatch),
    getCurrentUser: bindActionCreators(userActions.getCurrentUser, dispatch)
  }
}

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(withNamespaces()(Header));
