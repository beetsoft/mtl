import React, { Component } from 'react';
import CheckoutForm from './checkoutForm';
import {Elements} from 'react-stripe-elements';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { userActions } from '../../actions/user.actions';
import { assignedUserActions } from '../../actions/assignedUsers.actions';
import { withNamespaces } from "react-i18next";
import { withRouter } from "react-router";
import PropTypes from 'prop-types';

class RegistrationStepThree extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ''
    }
  }

  /*
  * Set payment in state if payment success.
  */
  successPayment = (id) => {
    const { t, data, address, hash } = this.props;
    data.stripeToken = id;
    let promise;
    if(hash){
      promise = this.props.registerByInvite(data, address, hash)
    } else {
      promise = this.props.registrationUser(data, address)
    }
    promise.then(response => {
      this.props.closeRegistrationWindow();
      this.props.history.push('/thanksPage');
    })
    .catch(error => {
      this.setState({ message: t('paymentFailed')});
    });

  }

  render(){
    const { message } = this.state;
    return(
      <div className="Registration__step-inner Registration__step-inner--wide">
        <Elements>
          <CheckoutForm successPayment={this.successPayment} />
        </Elements>
        {
          message && <div className="input__feedback error">{message}</div>
        }
      </div>
    )
  }
}

RegistrationStepThree.propTypes = {
  closeRegistrationWindow: PropTypes.func
};

function mapStateToProps(state) {
  const { user } = state;
  const address = user.get('address');
  const data = user.get('data').toJS();
  return { address, data };
}

function mapDispatchToProps(dispatch){
  return {
    setRegistrationStep: bindActionCreators(userActions.setRegistrationStep, dispatch),
    registrationUser: bindActionCreators(userActions.registrationUser, dispatch),
    registerByInvite: bindActionCreators(assignedUserActions.registerByInvite, dispatch)
  }
}

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(withNamespaces()(RegistrationStepThree));
