import React, {Component} from 'react';
import {CardNumberElement, CardExpiryElement, CardCVCElement, injectStripe} from 'react-stripe-elements';
import { withNamespaces } from "react-i18next";
import '../../assets/styles/PaymentCard.style.scss';
import PropTypes from 'prop-types';

class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
      inProcess: false
    }

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
  * Send form data for payment.
  * @param {Object} e - Target element.
  */
  handleSubmit(e){
    e.preventDefault();
    const  { t } = this.props;
    this.setState({inProcess: true});
    if (this.props.stripe) {
      this.props.stripe
        .createToken()
        .then((payload) => {
          if(payload.token){
            this.props.successPayment(payload.token.id);
          } else {
            const { code } = payload.error;
            this.setState({ message: t(code), inProcess: false })
          }
        });
    } else {
      this.setState({ message: t('stripeNotLoaded'), inProcess: false })
    }
  };

  render() {
    const { t } = this.props;
    const { inProcess, message } = this.state;
    return (
      <div className="PaymentCard">
      <div className="PaymentCard__face">
        <div className="PaymentCard__logos"></div>
        <form id="payment" className="PaymentCard__form" onSubmit={this.handleSubmit}>
          <CardNumberElement className="PaymentCard__input PaymentCard__input--number" />
          <CardExpiryElement className="PaymentCard__input" />
          <CardCVCElement className="PaymentCard__input PaymentCard__input--cvc"/>
        </form>

      </div>
      {
        message && <div className="input__feedback error">{message}</div>
      }
      <button className="mtl-button mtl-button--rounded mtl-button--orange-solid" disabled={inProcess} type="submit" form="payment">{t('pay')}</button>
    </div>
  )
  }
}

CheckoutForm.propTypes = {
  successPayment: PropTypes.func
}

export default withNamespaces()(injectStripe(CheckoutForm));
