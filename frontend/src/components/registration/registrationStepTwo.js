import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { userActions } from '../../actions/user.actions';
import { assignedUserActions } from '../../actions/assignedUsers.actions';
import { withNamespaces } from "react-i18next";
import { withRouter } from "react-router";
import PropTypes from 'prop-types';
import Autocomplete from 'react-google-autocomplete';
import {makeErrorPath} from '../../helpers/makeErrorPath';

class RegistrationStepTwo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      invalidAddress: false,
      errors: {}
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeAddress = this.handleChangeAddress.bind(this);
  }

  /**
  * Change membership plan.
  * @param {Boolean} value - True or false.
  */
  changeMembershipPlan(value){
    this.props.setUser({is_free: value});
  }

  placeSelected(place){
    const address = place.formatted_address;
    this.props.setAddress(address);
  }

  handleChangeAddress(e) {
    const address = e.target.value;
    this.props.setAddress(address);
    this.setState(prevState => ({
      errors: {
        address: null
      },
    }));
  }

  /**
  * Validate and send form data to store.
  * @param {Object} e - Target element.
  */
  handleSubmit(e){
    e.preventDefault();
    const { data, address, data: {is_free}, hash } = this.props;
    let promise;
    if(is_free){
      if(hash){
        promise = this.props.registerByInvite(data, address, hash);
      } else {
        promise = this.props.registrationUser(data, address);
      }
      promise.then(response => {
          this.props.closeRegistrationWindow();
          this.props.history.push('/thanksPage');
        })
        .catch(errors => {
          const errorData = makeErrorPath(errors.details || errors);
          errorData.map(error => {
            this.setState(prevState => ({
              errors: {
                ...prevState.errors,
                [error.label]: error.path
              }
            }));
            return true;
          })
        })
    } else {
      this.props.setUser(data);
      this.props.setAddress(address);
      this.props.setRegistrationStep(3);
    }
  }

  render(){
    const { t, registrationStep, data: {is_free}, address } = this.props;
    const { errors } = this.state;
    return(
      <div className="Registration__step-inner Registration__step-inner--wide">
        <div className="Registration__plans-row">
          <div className="Registration__plan">
            <div className={`Plan ${is_free ? 'Plan--selected' : ''}`}>
              <div className="Plan__icon"><span className="mtl-icon-plan-free"></span></div>
              <div className="Plan__name">{t('base')}</div>
              <div className="Plan__description">{t('baseMessage')}</div>
              <div className="Plan__price">{t('free')}</div>
              <button className="Plan__button mtl-button mtl-button--rounded mtl-button--sm" onClick={() => this.changeMembershipPlan(true)}>{t('select')}</button>
            </div>
          </div>
          <div className="Registration__plan">
            <div className={`Plan ${!is_free ? 'Plan--selected' : ''}`}>
              <div className="Plan__icon"><span className="mtl-icon-plan-premium"></span></div>
              <div className="Plan__name">{t('premium')}</div>
              <div className="Plan__description">{t('premiumMessage')}</div>
              <div className="Plan__price">$10.-</div>
              <button className="Plan__button mtl-button mtl-button--rounded mtl-button--sm" onClick={() => this.changeMembershipPlan(false)}>{t('select')}</button>
            </div>
          </div>
        </div>
        <div className="Form__row">
            <div className="input input--empty-value">
                <div className='input__field-wrap'>
                    <Autocomplete
                        className='input__field'
                        placeholder={t('enterAddress')}
                        onPlaceSelected={(place) => {this.placeSelected(place)}}
                        types={['address']}
                        value={address || ''}
                        onChange={this.handleChangeAddress}
                    />
                    { errors.address ? <div className="input__feedback error">{t(errors.address)}</div> : ''}
                </div>
            </div>
        </div>
        <div className="Registration__submit-row">
            <span className="Registration__total-steps">
              {t('stepOf', {currentStep: registrationStep, maxSteps: is_free === false ? '3' : '2'})}
            </span>
          <button className="mtl-button mtl-button--rounded mtl-button--orange-solid" onClick={this.handleSubmit}>{this.props.data.is_free ? t('sendData') : t('next')}</button>
        </div>
      </div>
    )
  }
}

RegistrationStepTwo.propTypes = {
  closeRegistrationWindow: PropTypes.func
};

function mapStateToProps(state){
  const { user } = state;
  const address = user.get('address');
  const data = user.get('data').toJS();
  const registrationStep = user.get('registrationStep');
  return { address, registrationStep, data };
}

function mapDispatchToProps(dispatch){
  return {
    setRegistrationStep: bindActionCreators(userActions.setRegistrationStep, dispatch),
    registrationUser: bindActionCreators(userActions.registrationUser, dispatch),
    setAddress: bindActionCreators(userActions.setAddress, dispatch),
    setUser: bindActionCreators(userActions.setUser, dispatch),
    registerByInvite: bindActionCreators(assignedUserActions.registerByInvite, dispatch)
  }
}

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(withNamespaces()(RegistrationStepTwo));
