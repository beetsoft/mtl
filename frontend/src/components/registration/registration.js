import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { userActions } from '../../actions/user.actions';
import { stepsClassNames } from '../../helpers/styles';
import RegistrationStepOne from './registrationStepOne';
import RegistrationStepTwo from './registrationStepTwo';
import RegistrationStepThree from './registrationStepThree';
import { withNamespaces } from "react-i18next";
import '../../assets/styles/registration.style.scss';
import PropTypes from 'prop-types';

class Registration extends Component {

  constructor(props){
    super(props);

    this.state = {
      steps: [
        RegistrationStepOne,
        RegistrationStepTwo,
        RegistrationStepThree
      ]
    }
  }

  /**
  * Change registration step.
  * @param {Number} step - Number of registration step.
  */
  changeStep(step){
    const { registrationStep } = this.props;
    if (registrationStep > step){
      this.props.setRegistrationStep(step);
    }
  }

  /**
  * Render current step.
  * @return {Object} Return necessary component.
  */
  renderCurrentStep(){
    const { registrationStep } = this.props;
    const Component = this.state.steps[registrationStep-1];
    return <Component closeRegistrationWindow={() => this.props.closeRegistrationWindow()} hash={this.props.hash}/>
  }

  /**
  * Render steps buttons.
  * @return {String} Return html blocks.
  */
  renderStepsButtons(){
    const { t, registrationStep } = this.props;
    return this.state.steps.map((component, index) => {
      index = index + 1;
      return (
        <div className={`Registration__indicator Registration__indicator--${registrationStep >= index ? 'showedStep' : stepsClassNames}`} onClick={() => {this.changeStep(index)}} key={index}>
          <span className="Registration__indicator-digit">{index}</span>
          <span>{t('step'+index)}</span>
        </div>
      )
    })
  }

  render(){
    const { t } = this.props;
    return (
      <div className="Registration">
        <div id="stepsContainer" className="Registration__indicators">
          {
            this.renderStepsButtons()
          }
        </div>
        <button className="modal-close-btn mtl-icon-cross" onClick={() => this.props.closeRegistrationWindow()}></button>
        <div className="Registration__steps">
          <h2 className="App-title App-title--small">{t('registration')}</h2>
          <div className="Registration__step">
            {
              this.renderCurrentStep()
            }
          </div>
        </div>

      </div>
    )
  }
}

Registration.propTypes = {
  closeRegistrationWindow: PropTypes.func
};

function mapStateToProps(state){
  const registrationStep = state.user.get('registrationStep');
  return { registrationStep };
}

function mapDispatchToProps(dispatch){
  return {
    setUser: bindActionCreators(userActions.setUser, dispatch),
    setRegistrationStep: bindActionCreators(userActions.setRegistrationStep, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(Registration));
