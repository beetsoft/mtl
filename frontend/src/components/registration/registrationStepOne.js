import React, { Component } from 'react';
import ReCAPTCHA from "react-google-recaptcha";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { userActions } from '../../actions/user.actions';
import { reCaptchaKey } from '../../config';
import { withNamespaces } from "react-i18next";
import _ from 'lodash';
import Input from '../helperComponents/input';
import * as Joi from 'joi-browser';

class RegistrationStepOne extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reCaptcha: false,
      errors: {
        email: '',
        firstname: '',
        lastname: '',
        password: '',
        confirmPassword: '',
        reCaptcha: ''
      }
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.reCaptchaChange = this.reCaptchaChange.bind(this);
    this.checkEmail = _.debounce(this.checkEmail, 500);

  }

  /**
  * Set user's data to state.
  * @param {Object} e - Target element.
  */
  handleChangeUser(e){
    const name = e.target.name;
    const value = e.target.value;
    this.props.setUser({[name]:value});
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        [name]: null
      }
    }));

    if(name === 'email'){
      this.checkEmail(value);
    }
  }

  /**
  * Checks email.
  * @param {String} value - Value of email.
  */
  checkEmail(value){
    this.props.checkEmail(value)
    .catch(errors => {
      errors.map(error => {
        this.setState(prevState => ({
          errors: {
            ...prevState.errors,
            email: error.path
          }
        }));
        return true;
      });
    });
  }

  /**
  * Change state for reCaptcha filed.
  */
  reCaptchaChange(){
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        reCaptcha: null
      },
      reCaptcha: true
    }));
  }

  /**
  * Validate and send form data to store.
  * @param {Object} e - Target element.
  */
  handleSubmit(e){
    e.preventDefault();
    e.persist();
    const { data, registrationStep } = this.props;
    const { reCaptcha } = this.state;
    data.reCaptcha = reCaptcha;
    delete data.is_free;
    Joi.validate(data, schema, {abortEarly: false})
    .then(response => {
      this.props.setUser(data);
      this.props.setRegistrationStep(registrationStep + 1);
    })
    .catch(errors => {
      errors.details.map(field => {
        const { type, context: {label} } = field;
        let path = 'errors.' + label + '.type.' + type;
        this.setState(prevState =>({
          errors: {
            ...prevState.errors,
            [label]: path
          }
        }))
        return true;
      })
    });
  }

  /**
  * Render input fields.
  * @return {String} Return html blocks.
  */
  renderFields(){
    const { data } = this.props;
    const { errors } = this.state;
    delete data.is_free;
    delete data.reCaptcha;
    const fields = Object.keys(data);
    return fields.map(field => {
      return (
        <Input
          type={field === 'password' || field === 'confirmPassword' ? 'password' : 'text'}
          name={field}
          handleChange={this.handleChangeUser.bind(this)}
          error={errors[field]}
          key={field}
          value={data[field]}
          placeholder={field}
        />
      );
    });
  }

  render(){
    const { t, registrationStep, data: {is_free} } = this.props;
    const { errors } = this.state;
    return(
      <div className="Registration__step-inner">
        <form encType="multipart/form-data" onSubmit={this.handleSubmit}>

          {
            this.renderFields()
          }
          < ReCAPTCHA
            sitekey={reCaptchaKey}
            onChange={this.reCaptchaChange}
          />

          {
            errors.reCaptcha && <div className="input__feedback error">{t('completeCaptcha')}</div>
          }

          <div className="Registration__submit-row">
            <span className="Registration__total-steps">
              {t('stepOf', {currentStep: registrationStep, maxSteps: is_free === false ? '3' : '2'})}
            </span>

            <button className="mtl-button mtl-button--rounded mtl-button--orange-solid" type="submit" >{t('next')}</button>
          </div>
        </form>
      </div>
    )
  }
}

const schema = {
  email: Joi.string().email().required(),
  firstname: Joi.string().min(3).max(30).required(),
  lastname: Joi.string().min(3).max(30).required(),
  password: Joi.string().min(6).max(30).required(),
  confirmPassword: Joi.string().valid(Joi.ref('password')).required(),
  reCaptcha: Joi.boolean().valid(true)
};

function mapStateToProps(state){
  const { user } = state;
  const data = user.get('data').toJS();
  const registrationStep = user.get('registrationStep');
  return { data, registrationStep };
}

function mapDispatchToProps(dispatch){
  return {
    setUser: bindActionCreators(userActions.setUser, dispatch),
    setRegistrationStep: bindActionCreators(userActions.setRegistrationStep, dispatch),
    checkEmail: bindActionCreators(userActions.checkEmail, dispatch),
    validateUserData: bindActionCreators(userActions.validateUserData, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNamespaces()(RegistrationStepOne));
