import React, { Component } from 'react';
import { withNamespaces } from "react-i18next";
import PropTypes from 'prop-types';

class Input extends Component {
  render(){
    const { type, name, error, t, icon, handleChange, value, placeholder } = this.props;
    return (
      <div className="Form__row">
          <div className="input input--empty-value">
              <div className={`input__field-wrap ${error ? 'error' : ''}`}>
                  <input
                    className={`input__field ${icon ? 'input__field--has-icon-left' : ''}`}
                    type={type}
                    placeholder={t(placeholder)}
                    onChange={handleChange}
                    name={name}
                    value={value}
                  />
                  {icon ? <span className={`input__icon mtl-icon-${icon}`}></span> : ''}
              </div>
              { error ? <div className="input__feedback error">{t(error)}</div> : ''}
          </div>
      </div>
    )
  }
}

Input.propTypes = {
  type: PropTypes.string,
  icon: PropTypes.string,
  placeholder: PropTypes.string,
  handleChange: PropTypes.func,
  name: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.string
};

export default withNamespaces()(Input);
