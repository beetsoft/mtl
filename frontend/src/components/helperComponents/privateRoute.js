import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { userActions } from '../../actions/user.actions';

function PrivateRoute({ component: Component, ...rest }) {
  const { token } = rest;
  const expiresIn = token ? Number(token.expiresIn) : '';
  if(expiresIn){
    if(Date.now() > expiresIn){
      rest.logout();
    }
  }

  return (
      <Route
        render={props =>
          token.key ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
}

function mapStateToProps(state) {
  const token = state.user.get('token').toJS();
  return { token }
}

function mapDispatchToProps(dispatch) {
  return {
    logout: bindActionCreators(userActions.logout, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
