import React from 'react';
import { connect } from 'react-redux';

class Alert extends React.Component{

  render(){
    const { alert } = this.props;
    if(alert.message){
      return (<div className={`alert ${alert.type}`}>{alert.message}</div>);
    } else {
      return '';
    }

  }
}

function mapStateToProps(state){
  const { alert } = state;
  return { alert };
}

export default connect(mapStateToProps)(Alert);
