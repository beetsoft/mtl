import React from 'react';
import '../../assets/styles/thanksPage.style.scss';
import { withNamespaces } from "react-i18next";

class ThanksPage extends React.Component{

  render(){
    const { t } = this.props;
      return (
        <div className="thanksPage">
          <div className="thanksPage__icon mtl-icon-done"></div>
          <h1 className="App-title App-title--small">
            {t('thanksFor')}
          </h1>
          <p className="thanksPage__text">
            {t('confirmEmailPage')}
          </p>
        </div>
      )

  }
}

export default withNamespaces()(ThanksPage);
