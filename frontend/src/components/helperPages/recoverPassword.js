import React, { Component } from 'react';
import { withNamespaces } from "react-i18next";
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { userActions } from '../../actions/user.actions';
import { withRouter } from "react-router";
import Input from '../helperComponents/input';
import PropTypes from 'prop-types';

class RecoverPassword extends Component {
  constructor(props){
    super(props);
    this.state = {
      password: '',
      confirmPassword: '',
      email: '',
      errors: {
        password: '',
        confirmPassword: '',
        email: ''
      },
      showMessage: false
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.sendRequest = this.sendRequest.bind(this);
  }

  /**
  * Set user's data to state.
  * @param {Object} e - Target element.
  */
  handleChange(e){
    const name = e.target.name;
    let value = e.target.value;

    this.setState(prevState => ({
      [name]: value,
      errors: {
        ...prevState.errors,
        [name]: null
      }
    }));
  }

  /**
  * Send form data for change password.
  * @param {Object} e - Target element.
  */
  handleSubmit(e){
    e.preventDefault();
    const { password, confirmPassword } = this.state;
    const { hash } = this.props;
    if(password === confirmPassword){
      this.props.changePassword({hash: hash, password: password}).then(response => {
        this.props.closeRecoverWindow();
        this.props.history.push('/');
      })
      .catch(errors => {
        errors.map(error => {
          let { label, path } = error;
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              [label]: path
            }
          }));
          return true;
        });
      });
    } else {
      this.setState(prevState => ({
        errors: {
          ...prevState.errors,
          confirmPassword: 'differentPassword'
        }
      }));
    }

  }

  /**
  * Send request for change password.
  * @param {Object} e - Target element.
  */
  sendRequest(e){
    e.preventDefault();
    const { email } = this.state;

    this.props.changePasswordRequest(email).then(response => {
      this.setState({showMessage: true});
    })
    .catch(errors => {
      errors.map(error => {
        this.setState(prevState => ({
          errors: {
            ...prevState.errors,
            email: error.path
          }
        }));
        return true;
      });
    });
  }

  render(){
    const { hash } = this.props;
    const { t } = this.props;
    const { showMessage } = this.state;
    const { email, password, confirmPassword } = this.state.errors;
    return (
      <div>
        {
          showMessage
          ? <div>{t('recoveryPasswordText')}</div>
          : <div>
            { !hash
              ? <div>
                {t('passwordRecovery')}
                  <form onSubmit={this.sendRequest}>
                  <span>{t('passwordRecoveryMessage')}</span>
                    <Input
                      type="text"
                      name="email"
                      handleChange={this.handleChange}
                      icon="mail"
                      error={email}
                      placeholder="email"
                    />
                    <div>
                      <button>{t('sendData')}</button>
                    </div>
                  </form>
                </div>
              : <div>
                  <form onSubmit={this.handleSubmit}>
                    <Input
                      type="password"
                      name="password"
                      handleChange={this.handleChange}
                      icon="key"
                      error={password}
                      placeholder="newPassword"
                    />
                    <Input
                      type="password"
                      name="confirmPassword"
                      handleChange={this.handleChange}
                      icon="key"
                      error={confirmPassword}
                      placeholder="confirmPassword"
                    />
                    <div>
                      <button>{t('save')}</button>
                    </div>
                  </form>
                </div>
            }
          </div>
        }
      </div>
    )
  }
}

RecoverPassword.propTypes = {
  closeRecoverWindow: PropTypes.func
};

function mapDispatchToProps(dispatch) {
  return {
    changePasswordRequest: bindActionCreators(userActions.changePasswordRequest, dispatch),
    changePassword: bindActionCreators(userActions.changePassword, dispatch)
  }
}

export default compose(
  withRouter,
  connect(null, mapDispatchToProps)
)(withNamespaces()(RecoverPassword));
