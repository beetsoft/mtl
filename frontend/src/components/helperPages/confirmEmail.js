import { Component } from 'react';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { userActions } from '../../actions/user.actions';
import { bindActionCreators } from 'redux';

class ConfirmEmail extends Component {
  constructor(props){
    super(props);
    this.state = {
      error: ''
    }
  }

  componentDidMount(){
    let search = new URLSearchParams(this.props.location.search);
    let hash = search.get("hash");
    this.props.confirmEmail(hash)
    .catch(errors => {
      errors.map(error => {
        this.setState({ error: error.path });
        return true;
      });
    });
  }

  render(){
    const { t } = this.props;
    const { error } = this.state;
    return (
      error
      ? t(error)
      : t('confirmEmailPage')
    );
  }

}

function mapDispatchToProps(dispatch){
  return {
    confirmEmail: bindActionCreators(userActions.confirmEmail, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(withNamespaces()(ConfirmEmail));
