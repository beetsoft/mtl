import React, { Component } from 'react';
import '../../assets/styles/ErrorPage.style.scss';
import { withNamespaces } from "react-i18next";

class ErrorPage extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="ErrorPage">
        <div className="ErrorPage__content">
          {t('badLuck')}
          <div className="ErrorPage__code">404</div>
            {t('pageSeemsNot')}
        </div>
      </div>
    )
  }
}

export default withNamespaces()(ErrorPage);
