import React, { Component } from 'react';
import Modal from 'react-modal';
import {modalWindow} from '../helpers/styles';
import Registration from './registration/registration';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      hash: ''
    }

    this.closeModal = this.closeModal.bind(this);
  }

  /**
  * Open modal window for registration.
  */
  openModal(){
    this.setState({modalIsOpen: true})
  }

  /**
  * Close modal window of registration.
  */
  closeModal(){
    this.setState({modalIsOpen: false});
    this.redirectToLogin();
  }

  componentDidMount() {
    const search = new URLSearchParams(this.props.location.search);
    const hash = search.get("hash");
    if (hash) {
      this.setState({hash: hash});
      this.openModal();
    }
  }

  /**
  * Redirect to login page.
  */
  redirectToLogin() {
    const { history } = this.props;
    history.push("/");
  }

  render() {
    const { user } = this.props;
    return (
      user.id
      ? <Redirect to="/" />
      : <Modal
          isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            style={modalWindow}
        >
            <Registration
                closeRegistrationWindow={() => this.closeModal()}
                hash={this.state.hash}
            />
        </Modal>
    );
  }
}

function mapStateToProps(state) {
  const user = state.user.get('data').toJS();
  return { user }
}

export default connect(mapStateToProps, null)(Invite);
