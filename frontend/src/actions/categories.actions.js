import { commonActions } from './common.actions';
import { categoriesConstants } from '../constants';
import { toDoListActions } from './toDoList.actions';

function createCategory(title, todo_list_id) {
  return dispatch => {
    dispatch(commonActions._dispatch('addCategoryToList', {title: title, todo_list_id: todo_list_id}))
    .then(response => {
      if(response.data){
        dispatch(setNewCategory(response.data))
        dispatch(toDoListActions.updateList({id: todo_list_id, category_id: response.data.id}))
      } else {
        dispatch(toDoListActions.updateList({id: todo_list_id, category_id: null}))
      }
    });
  }
}

function editCategory(category_id, title) {
  return dispatch => {
    dispatch(commonActions._dispatch('editCategory', {category_id: category_id, title: title}))
    .then(response => {
      dispatch(changeCategoryTitle(response.data))
    });
  }
}

function deleteCategory(category_id) {
  return dispatch => {
    dispatch(commonActions._dispatch('deleteCategory', {category_id: category_id}))
    .then(response => {
      dispatch(deletedCategory(category_id));
      dispatch(toDoListActions.getToDoLists());
    });
  }
}

function getUserCategories() {
  return dispatch => {
    dispatch(commonActions._dispatch('getUserCategories'))
    .then(response => {
      dispatch(setUserCategories(response.data))
    });
  }
}

function changeCategoryTitle(data) {
  return { type: categoriesConstants.CHANGE_CATEGORY_TITLE, data }
}

function setUserCategories(data) {
  return { type: categoriesConstants.SET_CATEGORIES, data }
}

function setNewCategory(data) {
  return { type: categoriesConstants.CREATE_CATEGORY, data }
}

function deletedCategory(id) {
  return { type: categoriesConstants.DELETE_CATEGORY, id }
}

export const categoriesActions = {
  createCategory,
  editCategory,
  deleteCategory,
  getUserCategories,
  changeCategoryTitle
}
