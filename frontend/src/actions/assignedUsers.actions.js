import { assignedUserConstants } from '../constants/assignedUserConstants';
import { commonActions } from './common.actions';

/**
* Get users by email.
* @param {String} email - Email of user.
*/
function getUsersByEmail(email) {
  return dispatch => {
    dispatch(commonActions._dispatch('getUsersByEmail', {email: email}))
    .then(response => {
      dispatch(setUsers(response.data))
    })
  }
}

/**
* Set data of users to store.
* @param {Object} data - Data of server response.
* @return {Object} Return action type and data.
*/
function setUsers(data) {
  return { type: assignedUserConstants.GET_USERS, data }
}

/**
* Clear data of users from store.
* @return {Object} Return action type.
*/
function clearUsers() {
  return { type: assignedUserConstants.CLEAR_ASSIGNED_USERS }
}

/**
* Share list to another user.
* @param {Number} id - List id.
* @param {String} email - Email of user.
* @return {Object} Return Promise Object.
*/
function shareList(id, email) {
  return dispatch => {
    return dispatch(commonActions._dispatch('shareList', {todo_list_id: id, email: email}))
  }
}

/**
* Delete shared list.
* @param {Number} user_id - user id.
* @param {Number} todo_list_id - List id.
*/
function deleteShare(user_id, todo_list_id, invited) {
  return dispatch => {
    dispatch(commonActions._dispatch('deleteShare', {todo_list_id: todo_list_id, user_id: user_id, invited: invited}))
  }
}

/**
* Register user by invite.
* @param {Object} data - Data of user.
* @param {String} address - User address.
* @param {String} hash - Hash of user invite.
* @return {Object} Return Promise Object.
*/
function registerByInvite(data, address, hash){
  const user = Object.assign({}, data, { address: address }, { hash: hash });
  delete user.confirmPassword;
  delete user.reCaptcha;
  return dispatch => {
    return dispatch(commonActions._dispatch('registerByInvite', user))
  }
}

export const assignedUserActions = {
  getUsersByEmail,
  clearUsers,
  shareList,
  registerByInvite,
  deleteShare
}
