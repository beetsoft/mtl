import { socket } from '../config';
import { userActions } from './user.actions';
import { makeErrorPath } from '../helpers/makeErrorPath';

/**
* Returns Promise Object of socket response.
* @param {String} event - Name of event.
* @param {Object} data - Data for request to socket.
* @return {Object} Return Promie Object.
*/
function _dispatch(event, data) {
  return dispatch => {
    return new Promise((resolve, reject) => {
      socket.emit(event, data, response => {
        if (response.isSuccess){
          const { data } = response;
          if(data && data.token){
            dispatch(userActions.setToken(data.token)).then(() => {
              socket.disconnect();
            });
          }
          resolve(response);
        } else {
          reject(makeErrorPath(response.data.errors ? response.data.errors : response.data))
        }
      })
    })
  }
}

export const commonActions = {
  _dispatch
}
