import { toDoListConstants } from '../constants';
import { commonActions } from './common.actions';
import { categoriesActions } from './categories.actions';
import { store } from '../index';

/**
* Add new list.
* @param {String} title - Title of list.
*/
function addNewToDoList(title, category) {
  return dispatch => {
    return dispatch(commonActions._dispatch('createList', {title: title}))
    .then(response => {
      dispatch(createList(response.data));
      if(category){
        dispatch(categoriesActions.createCategory(category, response.data.id))
      }
      return response.data.id;
    })
  }
}

/**
* Edit title of list.
* @param {Number} id - List id.
* @param {String} title - Title of list.
*/
function editListTitle(id, title) {
  return dispatch => {
    dispatch(commonActions._dispatch('editListTitle', {id: id, title: title}));
  }
}

/**
* Delete list.
* @param {Number} id - List id.
*/
function deleteToDoList(id) {
  return dispatch => {
    dispatch(commonActions._dispatch('deleteList', {id: id}));
  }
}

/**
* Set current list to store.
* @param {Object} data - Data of list.
* @return {Object} Return action type and data.
*/
function setCurrentToDoList(data) {
  return { type: toDoListConstants.GET_TO_DO_LIST, data }
}

/**
* Set all lists to store.
* @param {Object} data - Data of lists.
* @return {Object} Return action type and data.
*/
function setToDoLists(data) {
  return { type: toDoListConstants.GET_TO_DO_LISTS, data }
}

/**
* Get current list.
* @param {Number} id - List id.
*/
function getCurrentToDoList(id) {
  const list = store.getState().toDoList.toJS().toDoLists.filter( list => list.id === id )[0];
  return dispatch => {
    dispatch(setCurrentToDoList(list));
  }
}

/**
* Get all lists from server.
* @return {Object} Return Promise Object.
*/
function getToDoLists() {
  return dispatch => {
    return dispatch(commonActions._dispatch('getTodoLists'))
    .then(response => {
      dispatch(setToDoLists(response.data));
    })
  }
}

/**
* Clear current list in store.
* @return {Object} Return action type.
*/
function clearCurrentToDoList() {
  return { type: toDoListConstants.CLEAR_TO_DO_LIST }
}

/**
* Update list in store.
* @param {Object} data - Data of list.
* @return {Object} Return action type and data.
*/
function updateList(data) {
  return { type: toDoListConstants.UPDATE_LIST, data }
}

/**
* Delete list from store.
* @param {Object} data - Data of list.
* @return {Object} Return action type and data.
*/
function deleteList(id) {
  return { type: toDoListConstants.DELETE_LIST, id }
}

/**
* Create new list.
* @param {Object} data - Data of list.
* @return {Object} Return action type and data.
*/
function createList(data) {
  return { type: toDoListConstants.CREATE_LIST, data }
}

function deleteShare(data){
  return { type: toDoListConstants.DELETE_SHARE, data }
}

function duplicateList(id){
  return dispatch => {
    dispatch(commonActions._dispatch('duplicateList', {id: id}))
    .then( response => {
      dispatch(createList(response.data))
    });
  }
}

export const toDoListActions = {
  addNewToDoList,
  editListTitle,
  getCurrentToDoList,
  getToDoLists,
  setCurrentToDoList,
  clearCurrentToDoList,
  deleteToDoList,
  updateList,
  deleteList,
  createList,
  deleteShare,
  duplicateList
}
