import { alertConstants } from '../constants/alertConstants';

export const alertActions = {
    success,
    error,
    clear
};

/**
* Return data about success.
* @param {String} message - Text about success response.
* @return {Object} Return action type and text of response.
*/
function success(message) {
  return { type: alertConstants.SUCCESS, message };
}

/**
* Return data about error.
* @param {String} message - Text about error response.
* @return {Object} Return action type and text of response.
*/
function error(message) {
  return { type: alertConstants.ERROR, message };
}

/**
* Clear alert.
* @return {Object} Return action type.
*/
function clear() {
  return { type: alertConstants.CLEAR };
}
