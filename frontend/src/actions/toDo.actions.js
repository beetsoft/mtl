import { commonActions } from './common.actions';

/**
* Add new item of list.
* @param {String} text - Text of item.
*/
function addNewToDo(text, todo_list_id) {
  return dispatch => {
    dispatch(commonActions._dispatch('addTodo', {todo_list_id: todo_list_id, text: text}))
  }
}

/**
* Edit item text of list.
* @param {Number} itemId - Item id.
* @param {String} text - Text of item.
*/
function editToDoText(itemId, text) {
  return dispatch => {
    dispatch(commonActions._dispatch('editTodoText', {id: itemId, text: text}))
  }
}

/**
* Edit item status of list.
* @param {Number} itemId - Item id.
*/
function editToDoStatus(itemId) {
  return dispatch => {
    dispatch(commonActions._dispatch('editTodoStatus', {id: itemId}))
  }
}

/**
* Delete item of list.
* @param {Number} itemId - Item id.
*/
function deleteToDo(itemId) {
  return dispatch => {
    dispatch(commonActions._dispatch('deleteTodo', {id: itemId}))
  }
}

export const toDoActions = {
  addNewToDo,
  editToDoText,
  editToDoStatus,
  deleteToDo
}
