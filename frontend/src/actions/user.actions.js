import { userConstants } from '../constants';
import { commonActions } from './common.actions';
import { toDoListActions } from './toDoList.actions';
import { categoriesActions } from './categories.actions';

/**
 * Set user data to store.
 * @param {Object} data - User data.
 * @return {Object} Return action type and user data.
 */
function setUser(data) {
  return {
    type: userConstants.DATA,
    data
  }
}

/**
 * Set user address to store.
 * @param {Object} address - User address.
 * @return {Object} Return action type and user address.
 */
function setAddress(address) {
  return {
    type: userConstants.ADDRESS,
    address
  }
}

/**
 * Set regitration step to store.
 * @param {Number} step - Number of registration step.
 * @return {Object} Return action type and step.
 */
function setRegistrationStep(step) {
  return {
    type: userConstants.REGISTRATION_STEP,
    step
  }
}

/**
 * Check email and return Promise.
 * @param {String} email - Email of user.
 * @return {Object} Return Promie Object.
 */
function checkEmail(email) {
  return dispatch => {
    return dispatch(commonActions._dispatch('checkEmail', {
      email: email
    }))
  }
}

/**
 * Returns Promise Object of socket response for payment.
 * @param {String} token - Token id for payment.
 * @return {Object} Return Promie Object.
 */
function upgradeUserAccount(token) {
  return dispatch => {
    return dispatch(commonActions._dispatch('upgradeUserAccount', {
      token: token
    })).then(response => {
      if(response.isSuccess){
        dispatch(setUser({is_free: false}));
      }
    })
  }
}

/**
 * Confirm email.
 * @param {String} hash - Hash of user's email.
 * @return {Object} Return Promie Object.
 */
function confirmEmail(hash) {
  return dispatch => {
    return dispatch(commonActions._dispatch('confirmEmail', {
      hash: hash
    }))
  }
}

/**
 * Registers user.
 * @param {Object} userData - User data.
 * @return {Object} Return Promie Object.
 */
function registrationUser(data, address) {
  const user = Object.assign({}, data, {
    address: address
  });
  delete user.confirmPassword;
  delete user.reCaptcha;
  return dispatch => {
    return dispatch(commonActions._dispatch('registerUser', user))
  }
}

/**
 * Edit user info.
 * @param {Object} userData - Data of user.
 * @return {Object} Return Promie Object.
 */
function editUserInfo(userData) {
  return dispatch => {
    return dispatch(commonActions._dispatch('editUserInfo', userData))
      .then(response => {
        dispatch(setUser(response.data))
      })
  }
}

/**
 * Validate user data.
 * @param {Object} userData - User data.
 * @return {Object} Return Promie Object.
 */
function validateUserData(userData) {
  const user = Object.assign({}, userData);
  delete user.confirmPassword;
  return dispatch => {
    return dispatch(commonActions._dispatch('validateUser', user))
  }
}

/**
 * Login user to site.
 * @param {Object} data - User data.
 * @return {Object} Return Promie Object.
 */
function login(data) {
  return dispatch => {
    return dispatch(commonActions._dispatch('login', data))
  }
}

/**
 * Logout user from site.
 */
function logout() {
  return dispatch => {
    localStorage.removeItem('token');
    dispatch(removeToken());
    dispatch(clearUser());
  }
}

/**
 * Remove token from store.
 * @return {Object} Return action type.
 */
function removeToken() {
  return {
    type: userConstants.REMOVE_TOKEN
  }
}

/**
 * Change password request.
 * @param {String} email - Email of user.
 * @return {Object} Return Promie Object.
 */
function changePasswordRequest(email) {
  return dispatch => {
    return dispatch(commonActions._dispatch('changePasswordRequest', {
      email: email
    }))
  }
}

/**
 * Change password.
 * @param {Object} data - Data of user.
 * @return {Object} Return Promie Object.
 */
function changePassword(data) {
  return dispatch => {
    return dispatch(commonActions._dispatch('changePassword', data))
  }
}

/**
 * Confirm email request.
 * @param {String} email - Email of user.
 * @return {Object} Return Promie Object.
 */
function confirmEmailRequest(email) {
  return dispatch => {
    return dispatch(commonActions._dispatch('confirmEmailRequest', {
      email: email
    }));
  }
}

/**
 * Clear all data of user in store.
 * @return {Object} Return action type.
 */
function clearUser() {
  return {
    type: userConstants.CLEAR
  };
}

/**
 * Set token of user in store.
 * @return {Object} Return action type.
 */
function setToken(token) {
  return dispatch => {
    return new Promise((resolve) => {
      localStorage.setItem('token', JSON.stringify(token));
      dispatch({ type: userConstants.SET_TOKEN, token: token });
      if(token){
        dispatch(getCurrentUser());
      }
      return resolve();
    });
  }
}

function getCurrentUser() {
  return dispatch => {
    dispatch(commonActions._dispatch('getCurrentUser'))
    .then(response => {
      dispatch(setUser(response.data));
      dispatch(toDoListActions.getToDoLists());
      dispatch(categoriesActions.getUserCategories());
    });
  }
}

function makeFavorite(id) {
  return dispatch => {
    dispatch(commonActions._dispatch('editFavoriteStatus', {id: id}))
    .then(response => {
      dispatch(setFavorite(response.data))
    });
  }
}

function setFavorite(id) {
  return { type: userConstants.CHANGE_FAVORITE, id }
}

function addNewPassword(data) {
  return dispatch => {
    return dispatch(commonActions._dispatch('changePasswordFromProfile', {current_password: data.password, new_password: data.newPassword}));
  }
}

export const userActions = {
  setUser,
  setRegistrationStep,
  confirmEmail,
  registrationUser,
  checkEmail,
  validateUserData,
  upgradeUserAccount,
  clearUser,
  editUserInfo,
  login,
  changePasswordRequest,
  changePassword,
  confirmEmailRequest,
  logout,
  setToken,
  setAddress,
  getCurrentUser,
  makeFavorite,
  addNewPassword
};
