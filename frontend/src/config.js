import io from 'socket.io-client';
import {store} from './index.js';
import { userActions } from './actions/user.actions';
import { toDoListActions } from './actions/toDoList.actions';
import { environments } from './environment';

export const reCaptchaKey = process.env.REACT_APP_RE_CAPTCHA_KEY || environments.REACT_APP_RE_CAPTCHA_KEY;

let token = JSON.parse(localStorage.getItem('token'));
token = token ? token.key : '';
export const socket = io(process.env.REACT_APP_URL || environments.REACT_APP_URL, {
  query : {
    auth_token: token
  }
});

socket.on('disconnect', (data) => {
  const token = store.getState().user.get('token').toJS().key;
  socket.io.opts.query = {
    auth_token: token
  };
  socket.connect();
});

function updateAndGetList(data) {
  store.dispatch(toDoListActions.updateList(data));
}

socket.on('connect', () => {
  store.dispatch(userActions.setToken(JSON.parse(localStorage.getItem('token'))));
})

socket.on('todoUpdated', (data) => {
  store.dispatch(toDoListActions.updateList(data));
})
socket.on('todoDeleted', (data) => {
  updateAndGetList(data);
})
socket.on('todoCreated', (data) => {
  updateAndGetList(data);
})
socket.on('listEdited', (data) => {
  updateAndGetList(data);
})
socket.on('listDeleted', (id) => {
  store.dispatch(toDoListActions.deleteList(id));
})
socket.on('listShared', (data) => {
  const userId = store.getState().user.get('data').toJS().id;
  if(data.shared_user_id === userId){
    store.dispatch(toDoListActions.createList(data));
  } else {
    updateAndGetList(data);
  }

})
socket.on('shareDeleted', (data) => {
  const userId = store.getState().user.get('data').toJS().id;
  if(data.userId === userId){
    store.dispatch(toDoListActions.deleteList(data.todo_list_id));
  } else {
    store.dispatch(toDoListActions.deleteShare(data));
  }
})
